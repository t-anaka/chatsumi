﻿namespace Chatsumi
{
    partial class BouyomiForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BouyomiForm));
            this.label1 = new System.Windows.Forms.Label();
            this.comboVoice = new System.Windows.Forms.ComboBox();
            this.trackSpeed = new System.Windows.Forms.TrackBar();
            this.trackTone = new System.Windows.Forms.TrackBar();
            this.trackVolume = new System.Windows.Forms.TrackBar();
            this.trackCount = new System.Windows.Forms.TrackBar();
            this.checkSpeed = new System.Windows.Forms.CheckBox();
            this.checkTone = new System.Windows.Forms.CheckBox();
            this.checkVolume = new System.Windows.Forms.CheckBox();
            this.checkCount = new System.Windows.Forms.CheckBox();
            this.textServerPath = new System.Windows.Forms.TextBox();
            this.buttonOpenFile = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.labelSpeedCount = new System.Windows.Forms.Label();
            this.labelToneCount = new System.Windows.Forms.Label();
            this.labelVolumeCount = new System.Windows.Forms.Label();
            this.labelCountCount = new System.Windows.Forms.Label();
            this.buttonTest = new System.Windows.Forms.Button();
            this.openBouyomi = new System.Windows.Forms.OpenFileDialog();
            this.labelSpeed = new System.Windows.Forms.Label();
            this.tipCheck = new System.Windows.Forms.ToolTip(this.components);
            this.labelTone = new System.Windows.Forms.Label();
            this.labelVolume = new System.Windows.Forms.Label();
            this.labelCount = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.trackSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackTone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackVolume)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackCount)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(45, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "音声";
            // 
            // comboVoice
            // 
            this.comboVoice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboVoice.FormattingEnabled = true;
            this.comboVoice.Location = new System.Drawing.Point(81, 47);
            this.comboVoice.Name = "comboVoice";
            this.comboVoice.Size = new System.Drawing.Size(202, 20);
            this.comboVoice.TabIndex = 1;
            this.comboVoice.SelectedIndexChanged += new System.EventHandler(this.ComboVoice_SelectedIndexChanged);
            // 
            // trackSpeed
            // 
            this.trackSpeed.Enabled = false;
            this.trackSpeed.Location = new System.Drawing.Point(81, 85);
            this.trackSpeed.Maximum = 200;
            this.trackSpeed.Minimum = 50;
            this.trackSpeed.Name = "trackSpeed";
            this.trackSpeed.Size = new System.Drawing.Size(202, 45);
            this.trackSpeed.TabIndex = 6;
            this.trackSpeed.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackSpeed.Value = 120;
            this.trackSpeed.Scroll += new System.EventHandler(this.TrackSpeed_Scroll);
            // 
            // trackTone
            // 
            this.trackTone.Enabled = false;
            this.trackTone.Location = new System.Drawing.Point(81, 127);
            this.trackTone.Maximum = 200;
            this.trackTone.Minimum = 50;
            this.trackTone.Name = "trackTone";
            this.trackTone.Size = new System.Drawing.Size(202, 45);
            this.trackTone.TabIndex = 7;
            this.trackTone.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackTone.Value = 120;
            this.trackTone.Scroll += new System.EventHandler(this.TrackTone_Scroll);
            // 
            // trackVolume
            // 
            this.trackVolume.Enabled = false;
            this.trackVolume.Location = new System.Drawing.Point(81, 170);
            this.trackVolume.Maximum = 100;
            this.trackVolume.Name = "trackVolume";
            this.trackVolume.Size = new System.Drawing.Size(202, 45);
            this.trackVolume.TabIndex = 8;
            this.trackVolume.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackVolume.Value = 50;
            this.trackVolume.Scroll += new System.EventHandler(this.TrackVolume_Scroll);
            // 
            // trackCount
            // 
            this.trackCount.Enabled = false;
            this.trackCount.Location = new System.Drawing.Point(81, 212);
            this.trackCount.Maximum = 20;
            this.trackCount.Minimum = 1;
            this.trackCount.Name = "trackCount";
            this.trackCount.Size = new System.Drawing.Size(202, 45);
            this.trackCount.TabIndex = 9;
            this.trackCount.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackCount.Value = 3;
            this.trackCount.Scroll += new System.EventHandler(this.trackCount_Scroll);
            // 
            // checkSpeed
            // 
            this.checkSpeed.AutoSize = true;
            this.checkSpeed.Location = new System.Drawing.Point(26, 88);
            this.checkSpeed.Name = "checkSpeed";
            this.checkSpeed.Size = new System.Drawing.Size(15, 14);
            this.checkSpeed.TabIndex = 12;
            this.tipCheck.SetToolTip(this.checkSpeed, "デフォルトから変更");
            this.checkSpeed.UseVisualStyleBackColor = true;
            this.checkSpeed.CheckedChanged += new System.EventHandler(this.CheckSpeed_CheckedChanged);
            // 
            // checkTone
            // 
            this.checkTone.AutoSize = true;
            this.checkTone.Location = new System.Drawing.Point(26, 130);
            this.checkTone.Name = "checkTone";
            this.checkTone.Size = new System.Drawing.Size(15, 14);
            this.checkTone.TabIndex = 13;
            this.tipCheck.SetToolTip(this.checkTone, "デフォルトから変更");
            this.checkTone.UseVisualStyleBackColor = true;
            this.checkTone.CheckedChanged += new System.EventHandler(this.CheckTone_CheckedChanged);
            // 
            // checkVolume
            // 
            this.checkVolume.AutoSize = true;
            this.checkVolume.Location = new System.Drawing.Point(26, 172);
            this.checkVolume.Name = "checkVolume";
            this.checkVolume.Size = new System.Drawing.Size(15, 14);
            this.checkVolume.TabIndex = 14;
            this.tipCheck.SetToolTip(this.checkVolume, "デフォルトから変更");
            this.checkVolume.UseVisualStyleBackColor = true;
            this.checkVolume.CheckedChanged += new System.EventHandler(this.CheckVolume_CheckedChanged);
            // 
            // checkCount
            // 
            this.checkCount.AutoSize = true;
            this.checkCount.Location = new System.Drawing.Point(26, 214);
            this.checkCount.Name = "checkCount";
            this.checkCount.Size = new System.Drawing.Size(15, 14);
            this.checkCount.TabIndex = 15;
            this.tipCheck.SetToolTip(this.checkCount, "デフォルトから変更");
            this.checkCount.UseVisualStyleBackColor = true;
            this.checkCount.CheckedChanged += new System.EventHandler(this.CheckCount_CheckedChanged);
            // 
            // textServerPath
            // 
            this.textServerPath.ForeColor = System.Drawing.Color.Black;
            this.textServerPath.Location = new System.Drawing.Point(81, 12);
            this.textServerPath.Name = "textServerPath";
            this.textServerPath.Size = new System.Drawing.Size(202, 19);
            this.textServerPath.TabIndex = 14;
            this.textServerPath.TextChanged += new System.EventHandler(this.TextServerPath_TextChanged);
            // 
            // buttonOpenFile
            // 
            this.buttonOpenFile.Location = new System.Drawing.Point(289, 10);
            this.buttonOpenFile.Name = "buttonOpenFile";
            this.buttonOpenFile.Size = new System.Drawing.Size(26, 23);
            this.buttonOpenFile.TabIndex = 16;
            this.buttonOpenFile.Text = "...";
            this.buttonOpenFile.UseVisualStyleBackColor = true;
            this.buttonOpenFile.Click += new System.EventHandler(this.ButtonOpenFile_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 12);
            this.label2.TabIndex = 17;
            this.label2.Text = "実行ファイル";
            // 
            // labelSpeedCount
            // 
            this.labelSpeedCount.Enabled = false;
            this.labelSpeedCount.Location = new System.Drawing.Point(287, 85);
            this.labelSpeedCount.Name = "labelSpeedCount";
            this.labelSpeedCount.Size = new System.Drawing.Size(28, 18);
            this.labelSpeedCount.TabIndex = 18;
            this.labelSpeedCount.Text = "120";
            this.labelSpeedCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelToneCount
            // 
            this.labelToneCount.Enabled = false;
            this.labelToneCount.Location = new System.Drawing.Point(287, 127);
            this.labelToneCount.Name = "labelToneCount";
            this.labelToneCount.Size = new System.Drawing.Size(28, 18);
            this.labelToneCount.TabIndex = 19;
            this.labelToneCount.Text = "120";
            this.labelToneCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelVolumeCount
            // 
            this.labelVolumeCount.Enabled = false;
            this.labelVolumeCount.Location = new System.Drawing.Point(287, 170);
            this.labelVolumeCount.Name = "labelVolumeCount";
            this.labelVolumeCount.Size = new System.Drawing.Size(28, 18);
            this.labelVolumeCount.TabIndex = 20;
            this.labelVolumeCount.Text = "50";
            this.labelVolumeCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelCountCount
            // 
            this.labelCountCount.Enabled = false;
            this.labelCountCount.Location = new System.Drawing.Point(287, 212);
            this.labelCountCount.Name = "labelCountCount";
            this.labelCountCount.Size = new System.Drawing.Size(28, 18);
            this.labelCountCount.TabIndex = 21;
            this.labelCountCount.Text = "3";
            this.labelCountCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonTest
            // 
            this.buttonTest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonTest.Location = new System.Drawing.Point(241, 246);
            this.buttonTest.Name = "buttonTest";
            this.buttonTest.Size = new System.Drawing.Size(75, 23);
            this.buttonTest.TabIndex = 22;
            this.buttonTest.Text = "テスト";
            this.buttonTest.UseVisualStyleBackColor = true;
            this.buttonTest.Click += new System.EventHandler(this.ButtonTest_Click);
            // 
            // openBouyomi
            // 
            this.openBouyomi.DefaultExt = "exe";
            this.openBouyomi.Filter = "実行ファイル|*.exe|全て|*.*";
            this.openBouyomi.Title = "棒読みちゃんの実行ファイルを開く";
            // 
            // labelSpeed
            // 
            this.labelSpeed.AutoSize = true;
            this.labelSpeed.Enabled = false;
            this.labelSpeed.Location = new System.Drawing.Point(47, 88);
            this.labelSpeed.Name = "labelSpeed";
            this.labelSpeed.Size = new System.Drawing.Size(29, 12);
            this.labelSpeed.TabIndex = 23;
            this.labelSpeed.Text = "速度";
            // 
            // labelTone
            // 
            this.labelTone.AutoSize = true;
            this.labelTone.Enabled = false;
            this.labelTone.Location = new System.Drawing.Point(47, 130);
            this.labelTone.Name = "labelTone";
            this.labelTone.Size = new System.Drawing.Size(29, 12);
            this.labelTone.TabIndex = 24;
            this.labelTone.Text = "音程";
            // 
            // labelVolume
            // 
            this.labelVolume.AutoSize = true;
            this.labelVolume.Enabled = false;
            this.labelVolume.Location = new System.Drawing.Point(47, 174);
            this.labelVolume.Name = "labelVolume";
            this.labelVolume.Size = new System.Drawing.Size(29, 12);
            this.labelVolume.TabIndex = 25;
            this.labelVolume.Text = "音量";
            // 
            // labelCount
            // 
            this.labelCount.AutoSize = true;
            this.labelCount.Enabled = false;
            this.labelCount.Location = new System.Drawing.Point(47, 214);
            this.labelCount.Name = "labelCount";
            this.labelCount.Size = new System.Drawing.Size(29, 12);
            this.labelCount.TabIndex = 26;
            this.labelCount.Text = "制限";
            // 
            // BouyomiForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(328, 281);
            this.Controls.Add(this.labelCount);
            this.Controls.Add(this.labelVolume);
            this.Controls.Add(this.labelTone);
            this.Controls.Add(this.labelSpeed);
            this.Controls.Add(this.buttonTest);
            this.Controls.Add(this.labelCountCount);
            this.Controls.Add(this.labelVolumeCount);
            this.Controls.Add(this.labelToneCount);
            this.Controls.Add(this.labelSpeedCount);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.buttonOpenFile);
            this.Controls.Add(this.checkCount);
            this.Controls.Add(this.textServerPath);
            this.Controls.Add(this.checkVolume);
            this.Controls.Add(this.checkTone);
            this.Controls.Add(this.checkSpeed);
            this.Controls.Add(this.trackCount);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.trackVolume);
            this.Controls.Add(this.comboVoice);
            this.Controls.Add(this.trackSpeed);
            this.Controls.Add(this.trackTone);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BouyomiForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "棒読みちゃんの設定";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.trackSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackTone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackVolume)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackCount)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboVoice;
        private System.Windows.Forms.TrackBar trackSpeed;
        private System.Windows.Forms.TrackBar trackTone;
        private System.Windows.Forms.TrackBar trackVolume;
        private System.Windows.Forms.TrackBar trackCount;
        private System.Windows.Forms.CheckBox checkSpeed;
        private System.Windows.Forms.CheckBox checkTone;
        private System.Windows.Forms.CheckBox checkVolume;
        private System.Windows.Forms.CheckBox checkCount;
        private System.Windows.Forms.TextBox textServerPath;
        private System.Windows.Forms.Button buttonOpenFile;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelSpeedCount;
        private System.Windows.Forms.Label labelToneCount;
        private System.Windows.Forms.Label labelVolumeCount;
        private System.Windows.Forms.Label labelCountCount;
        private System.Windows.Forms.Button buttonTest;
        private System.Windows.Forms.OpenFileDialog openBouyomi;
        private System.Windows.Forms.Label labelSpeed;
        private System.Windows.Forms.ToolTip tipCheck;
        private System.Windows.Forms.Label labelTone;
        private System.Windows.Forms.Label labelVolume;
        private System.Windows.Forms.Label labelCount;
    }
}