﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chatsumi
{
    public enum CommentType
    {
        Normal, Member, Moderator, Owner, Superchat, Membership, Sticker
    }

    public static class CommentTypeExt
    {
        public static string GetString(this CommentType type)
        {
            switch (type)
            {
                case CommentType.Normal: return "normal";
                case CommentType.Member: return "member";
                case CommentType.Moderator: return "moderator";
                case CommentType.Owner: return "owner";
                case CommentType.Superchat: return "superchat";
                case CommentType.Membership: return "membership";
                case CommentType.Sticker: return "sticker";
            }
            return "unknown";
        }
    }

    public class CommentData
    {
        public string Id { get; set; }
        public CommentType Type { get; set; }
        public string Photo { get; set; }
        public DateTime Time { get; set; }
        public string Name { get; set; }
        public BadgeData[] Badge { get; set; }
        public string Text { get; set; }
        public string Html { get; set; }
        public string Amount { get; set; }
        public string Sticker { get; set; }
    }

    public class BadgeData
    {
        public string Type { get; set; }
        public string Src { get; set; }
    }
}
