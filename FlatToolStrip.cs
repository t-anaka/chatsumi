﻿using System.Windows.Forms;

public class FlatToolStrip : ToolStrip
{
    public FlatToolStrip()
    {
        Renderer = new ToolStripProfessionalRenderer()
        {
            RoundedEdges = false,
        };
        GripStyle = ToolStripGripStyle.Hidden;
    }
}
