﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharp;
using System.Diagnostics;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.WebSockets;

namespace Chatsumi
{
    public partial class MainForm : Form
    {
        Config config = new Config();
        SyncTimer syncTimer = new SyncTimer();
        Chromium chromPick = new Chromium();
        Chromium chromChat = new Chromium();
        YouTubeUtil ytUtil = new YouTubeUtil();
        Speech speech = new Speech();
        Bouyomi bouyomi = new Bouyomi();
        NameForm nameForm = null;
        VoiceForm voiceForm = null;
        BouyomiForm bouyomiForm = null;
        NGWordForm ngWordForm = null;
        FontForm fontForm = null;
        WebScoketForm webSocketForm = null;
        LogForm logForm = null;
        LogWriter logChat = new LogWriter();
        LogWriter logPick = new LogWriter();
        WebSocketServer socketServer = new WebSocketServer();

        //List<CommentData> commentBuffer = new List<CommentData>();
        bool collapsed = false;
        bool clearCache = false;
        ConnectData connectData;
        string appName = "";

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            LoadConfig();

            appName = Path.GetFileNameWithoutExtension(Application.ExecutablePath);
            Text = appName;

            ProcUtil.KillZonbieProcess("CefSharp.BrowserSubprocess");
            Chromium.Initialize();

            SuspendLayout();

            syncTimer.Elapsed += SyncTimer_Elapsed;

            chromChat.Init(splitMain.Panel2);
            chromChat.FrameLoadEnd += ChromeChat_FrameLoadEnd;

            chromPick.Init(splitMain.Panel1, Path.Combine(Application.StartupPath, "html", "pickup.html"));
            chromPick.FrameLoadEnd += ChromePick_FrameLoadEnd;

            InitZoom();

            if (config.Pickup.AutoOpen)
                ClosePickup();

            sepaDevTool.Visible = config.DevTool.Enable;
            menuDevTool.Visible = config.DevTool.Enable;

            ResumeLayout();
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                socketServer.Dispose();
                syncTimer.Dispose();

                nameForm?.Dispose();
                chromPick.Dispose();
                chromChat.Dispose();

                speech.Dispose();
                bouyomi.Dispose();
                logChat.Dispose();
                logPick.Dispose();
            }
            finally
            {
                Chromium.Shutdown();
                SaveConfig();
            }

            if (clearCache)
            {
                try { Directory.Delete("cache", true); }
                catch (Exception ex) { Debug.WriteLine(ex.Message); }
            }
        }

        private void LoadConfig(bool initPos = true)
        {
            config = JsonUtil.LoadAsJson("config.json", new Config());

            var asm = System.Reflection.Assembly.GetExecutingAssembly();
            var ver = asm.GetName().Version.ToString();
            if (config.Ver != ver && config.Ver != "")
            {
            }
            config.Ver = ver;

            syncTimer.Interval = config.Main.UpdateInterval;

            bouyomi.Config = config.Speech.Bouyomi;
            if (config.Speech.UseBouyomi)
            {
                if (!bouyomi.StartServer())
                {
                    config.Speech.UseBouyomi = false;
                    menuUseBouyomi.Checked = false;
                    MsgBox.Show(this, MsgBoxIcon.Caution, MsgBoxButton.OK, MsgBoxSound.Asterisk, "エラー", "棒読みちゃんを起動出来ませんでした。");
                }
            }

            speech.Init(this);
            speech.Config = config.Speech.Voice;

            logChat.FontName = config.Font.Name;
            logChat.Limit = config.Log.Limit;
            logChat.TitleLen = config.Log.TitleLen;

            logPick.FontName = config.Font.Name;
            logPick.Limit = config.Log.Limit;
            logPick.TitleLen = config.Log.TitleLen;

            menuUserName.Checked = config.Pickup.UserName;
            menuOwner.Checked = config.Pickup.Owner;
            menuModeretor.Checked = config.Pickup.Moderator;
            menuMember.Checked = config.Pickup.Member;
            menuSuperChat.Checked = config.Pickup.SuperChat;
            menuMembership.Checked = config.Pickup.Membership;
            menuSticker.Checked = config.Pickup.Sticker;
            menuAutoOpen.Checked = config.Pickup.AutoOpen;

            menuSpeechOff.Checked = config.Speech.Mode == SpeechMode.Off;
            menuSpeechPick.Checked = config.Speech.Mode == SpeechMode.Pick;
            menuSpeechAll.Checked = config.Speech.Mode == SpeechMode.All;
            menuUseBouyomi.Checked = config.Speech.UseBouyomi;

            menuEnableLog.Checked = config.Log.Enable;

            menuTopMost.Checked = config.Main.TopMost;
            TopMost = config.Main.TopMost;

            if (initPos)
            {
                if (config.Main.Position != Rectangle.Empty)
                {
                    Location = config.Main.Position.Location;
                    Size = config.Main.Position.Size;
                }
                if (config.Main.Splitter > 0)
                {
                    splitMain.SplitterDistance = config.Main.Splitter;
                }
            }
        }

        private void SaveConfig()
        {
            config.Main.Position = new Rectangle(Location, Size);
            config.Main.Splitter = splitMain.SplitterDistance;
            JsonUtil.SaveAsJson("config.json", config);
        }

        private void InitZoom()
        {
            var menus = new List<ToolStripMenuItem>();
            for (var i = 50; i <= 250; i += 10)
            {
                var zoom = i * 0.01;
                var menu = new ToolStripMenuItem();
                menu.Name = $"menuZoom{i}";
                menu.Text = $"{i}%";
                menu.Tag = zoom;
                menu.Checked = zoom == config.Main.Zoom ? true : false;
                menu.Click += MenuZoom_Click;
                menus.Add(menu);
            }
            menuZoom.DropDownItems.AddRange(menus.ToArray());
        }

        private async Task Connect()
        {
            if (textUrl.Text == "")
                return;

            connectData = ytUtil.GetConnectData(textUrl.Text);
            if (connectData == null)
            {
                textUrl.Text = "";
                return;
            }

            Text = $"{appName} - 読込中...";
            await syncTimer.Stop();

            chromPick.ExecuteJavaScriptAsync("clearItem()");
            ClosePickup();

            chromChat.WebBrowser.Load(ytUtil.GetChatUrl(connectData.Id));

            if (socketServer.IsListening)
            {
                var chatData = new ChatData
                {
                    Type = ChatType.Connect,
                    Data = connectData,
                };
                var json = JsonUtil.ToJson(chatData);
                await socketServer.Send(json);
            }
        }

        private void ChromePick_FrameLoadEnd(object sender, FrameLoadEndEventArgs e)
        {
            chromPick.SetFont(config.Font.Name);
            chromPick.SetZoom(config.Main.Zoom);
        }

        private async void ChromeChat_FrameLoadEnd(object sender, FrameLoadEndEventArgs e)
        {
            if (e.Url.StartsWith("https://www.youtube.com/live_chat?"))
            {
                Invoke((MethodInvoker)(() => Text = $"{appName} - {connectData.Title}"));

                chromChat.SetFont(config.Font.Name);
                chromChat.SetZoom(config.Main.Zoom);

                var script = File.ReadAllText("js/getChat.js");
                chromChat.SetScript(script);

                var css = (string)await chromChat.EvaluateScriptAsync("getCss()");
                try
                {
                    File.WriteAllText("html/style.css", css);
                    chromPick.ExecuteJavaScriptAsync("setStyle(\"style.css\")");
                }
                catch { }

                await logChat.Open(chromChat, connectData.Title, connectData.Id, "c");
                await logPick.Open(chromChat, connectData.Title, connectData.Id, "p");

                if (logPick.Comments.Count > 0)
                {
                    OpenPickup();
                    var html = string.Join("\n", logPick.Comments.Select(a => a.Html));
                    html = chromChat.ReplaceScript(html);
                    chromPick.ExecuteJavaScriptAsync($"addItem(\"{html}\")");
                }

                syncTimer.Start();
            }
        }

        #region Update
        private bool IsPickup(CommentData comment)
        {
            if (config.Pickup.UserName)
            {
                if (config.Pickup.PickupName.Names.Contains(comment.Name))
                    return true;
            }
            if (config.Pickup.Owner)
            {
                if (comment.Type == CommentType.Owner)
                    return true;
            }
            if (config.Pickup.Moderator)
            {
                if (comment.Type == CommentType.Moderator)
                    return true;
            }
            if (config.Pickup.Member)
            {
                if (comment.Type == CommentType.Member)
                    return true;
            }
            if (config.Pickup.SuperChat)
            {
                if (comment.Type == CommentType.Superchat)
                    return true;
            }
            if (config.Pickup.Membership)
            {
                if (comment.Type == CommentType.Membership)
                    return true;
            }
            if (config.Pickup.Sticker)
            {
                if (comment.Type == CommentType.Sticker)
                    return true;
            }
            return false;
        }

        private void Speech(CommentData comment)
        {
            if (config.Speech.Mode == 0)
                return;
            if (config.Speech.NGWord.Words.Exists(word => comment.Text.Contains(word)))
                return;
            if (config.Speech.UseBouyomi)
            {
                if (!bouyomi.Speak(comment.Text))
                {
                    Invoke((MethodInvoker)(() => menuUseBouyomi.Checked = false));
                    config.Speech.UseBouyomi = false;
                    MsgBox.Show(this, MsgBoxIcon.Caution, MsgBoxButton.OK, MsgBoxSound.Asterisk, "エラー", "棒読みちゃんが停止しました。");
                }
            }
            else
            {
                speech.Speak(comment.Text);
            }
        }

        private async Task UpdateData()
        {
            var data = (string)await chromChat.EvaluateScriptAsync("getChat()");
            if (data == null)
                return;

            var list = JsonConvert.DeserializeObject<CommentData[]>(data);
            if (list.Length == 0)
                return;

            var comments = new List<CommentData>(list);

            var find = comments.FindIndex(a => a.Id == logChat.LastId);
            if (find > -1)
                comments.RemoveRange(0, find + 1);

            if (comments.Count == 0)
                return;

            var interval = (int)Math.Max(0, syncTimer.Interval / comments.Count);

            foreach (var comment in comments)
            {
                if (IsPickup(comment))
                {
                    if (config.Pickup.AutoOpen)
                        OpenPickup();
                    if (!logPick.Comments.Exists(a => a.Id == comment.Id))
                    {
                        logPick.Append(comment);

                        chromPick.ExecuteJavaScriptAsync($"addItem('{comment.Html}')");

                        if (config.Speech.Mode == SpeechMode.Pick ||
                            config.Speech.Mode == SpeechMode.All)
                            Speech(comment);
                    }
                }
                else
                {
                    if (config.Speech.Mode == SpeechMode.All)
                        Speech(comment);
                }

                logChat.Append(comment);
                if (socketServer.IsListening)
                {
                    var chatData = new ChatData
                    {
                        Type = ChatType.Comment,
                        Data = comment,
                    };
                    var json = JsonUtil.ToJson(chatData);
                    await socketServer.Send(json);
                }
                if (config.Speech.Mode > SpeechMode.Off && comments.Count <= 10)
                    Thread.Sleep(interval);
            }
        }

        private async void SyncTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                await UpdateData();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }
        #endregion

        #region Toggle Pickup
        private void OpenPickup()
        {
            if (!collapsed)
                return;
            Invoke((MethodInvoker)(() =>
            {
                splitMain.Panel1Collapsed = false;
                collapsed = false;
            }));
        }
        private void ClosePickup()
        {
            if (collapsed)
                return;
            Invoke((MethodInvoker)(() =>
            {
                splitMain.Panel1Collapsed = true;
                collapsed = true;
            }));
        }
        #endregion

        #region Menu Connect
        private async void ButtonPower_Click(object sender, EventArgs e)
        {
            await Connect();
        }

        private async void TextUrl_EnterKeyDown(object sender, KeyEventArgs e)
        {
            await Connect();
        }

        private async void TextUrl_DragDropText(object sender, string e)
        {
            await Connect();
        }
        #endregion

        #region Menu Pickup
        private void MenuUserName_CheckedChanged(object sender, EventArgs e)
        {
            config.Pickup.UserName = menuUserName.Checked;
        }
        private void MenuOwner_CheckedChanged(object sender, EventArgs e)
        {
            config.Pickup.Owner = menuOwner.Checked;
        }
        private void MenuModeretor_CheckedChanged(object sender, EventArgs e)
        {
            config.Pickup.Moderator = menuModeretor.Checked;
        }
        private void MenuMember_CheckedChanged(object sender, EventArgs e)
        {
            config.Pickup.Member = menuMember.Checked;
        }
        private void MenuSuperChat_CheckedChanged(object sender, EventArgs e)
        {
            config.Pickup.SuperChat = menuSuperChat.Checked;
        }
        private void MenuMembership_CheckedChanged(object sender, EventArgs e)
        {
            config.Pickup.Membership = menuMembership.Checked;
        }
        private void MenuSticker_CheckedChanged(object sender, EventArgs e)
        {
            config.Pickup.Sticker = menuSticker.Checked;
        }
        private void MenuEditUserName_Click(object sender, EventArgs e)
        {
            if (nameForm != null)
                if (!nameForm.IsDisposed)
                    return;
            nameForm = new NameForm(config.Pickup.PickupName);
            nameForm.Show(this);
        }
        private void MenuAutoOpen_CheckedChanged(object sender, EventArgs e)
        {
            menuAutoOpen.Checked = config.Pickup.AutoOpen;
        }
        private void MenuTogglePick_Click(object sender, EventArgs e)
        {
            if (collapsed)
                OpenPickup();
            else
                ClosePickup();
        }
        #endregion

        #region Menu Speech
        private void MenuSpeechOff_Click(object sender, EventArgs e)
        {
            menuSpeechOff.Checked = true;
            menuSpeechPick.Checked = false;
            menuSpeechAll.Checked = false;
            config.Speech.Mode = SpeechMode.Off;
        }
        private void MenuSpeechPick_Click(object sender, EventArgs e)
        {
            menuSpeechOff.Checked = false;
            menuSpeechPick.Checked = true;
            menuSpeechAll.Checked = false;
            config.Speech.Mode = SpeechMode.Pick;
        }
        private void MenuSpeechAll_Click(object sender, EventArgs e)
        {
            menuSpeechOff.Checked = false;
            menuSpeechPick.Checked = false;
            menuSpeechAll.Checked = true;
            config.Speech.Mode = SpeechMode.All;
        }
        private void MenuNGWord_Click(object sender, EventArgs e)
        {
            if (ngWordForm != null)
                if (!ngWordForm.IsDisposed)
                    return;
            ngWordForm = new NGWordForm(config.Speech.NGWord);
            ngWordForm.Show(this);
        }
        private void MenuEditVoice_Click(object sender, EventArgs e)
        {
            if (voiceForm != null)
                if (!voiceForm.IsDisposed)
                    return;
            voiceForm = new VoiceForm(config.Speech.Voice, speech);
            voiceForm.Show(this);
        }
        private void MenuUseBouyomi_CheckedChanged(object sender, EventArgs e)
        {
            config.Speech.UseBouyomi = menuUseBouyomi.Checked;
            if (menuUseBouyomi.Checked)
            {
                if (!bouyomi.StartServer())
                {
                    config.Speech.UseBouyomi = false;
                    menuUseBouyomi.Checked = false;
                    MsgBox.Show(this, MsgBoxIcon.Caution, MsgBoxButton.OK, MsgBoxSound.Asterisk, "エラー", "棒読みちゃんを起動出来ませんでした。");
                }
            }
        }
        private void MenuEditBouyomi_Click(object sender, EventArgs e)
        {
            if (bouyomiForm != null)
                if (!bouyomiForm.IsDisposed)
                    return;
            bouyomiForm = new BouyomiForm(config.Speech.Bouyomi, bouyomi);
            bouyomiForm.Show(this);
        }
        #endregion

        #region Menu Log
        private void MenuEnableLog_CheckedChanged(object sender, EventArgs e)
        {
            config.Log.Enable = menuEnableLog.Checked;
        }

        private void OpenLog(LogWriter log)
        {
            if (log.Comments.Count == 0)
                return;

            if (logForm != null)
                if (!logForm.IsDisposed)
                    logForm.Dispose();

            var list = log.CreateTemp();
            if (list.Length == 0)
            {
                log.DeleteTemp();
                return;
            }

            logForm = new LogForm(config, connectData.Title, list);
            logForm.FormClosed += (a, b) =>
            {
                log.DeleteTemp();
            };

            logForm.Show(this);
        }

        private void MenuOpenChatLog_Click(object sender, EventArgs e)
        {
            OpenLog(logChat);
        }

        private void MenuOpenPickLog_Click(object sender, EventArgs e)
        {
            OpenLog(logPick);
        }

        private void MenuOpenLogPath_Click(object sender, EventArgs e)
        {
            var path = "";
            if (string.IsNullOrEmpty(logChat.ShortTitle))
                path = logChat.LogDir;
            else
                path = Path.Combine(logChat.LogDir, logChat.ShortTitle);
            if (Directory.Exists(path))
                Process.Start(path);
        }

        private async void MenuZipLog_Click(object sender, EventArgs e)
        {
            var files = await logChat.Zip();
            MsgBox.Show(this, MsgBoxIcon.Info, MsgBoxButton.OK, MsgBoxSound.Asterisk,
                "過去のログを圧縮", $"過去のログを圧縮しました。({files.Length}ファイル)");
        }
        #endregion

        #region Menu Etc
        private void MenuTopMost_CheckedChanged(object sender, EventArgs e)
        {
            config.Main.TopMost = menuTopMost.Checked;
            TopMost = menuTopMost.Checked;
        }
        private async void MenuWebSocket_CheckedChanged(object sender, EventArgs e)
        {
            if (menuWebScoket.Checked)
            {
                var url = $"http://{config.WebSocket.Host}:{config.WebSocket.Port}/";
                socketServer.Start(url);

                if (connectData != null)
                {
                    var chatData = new ChatData
                    {
                        Type = ChatType.Connect,
                        Data = connectData,
                    };
                    var json = JsonUtil.ToJson(chatData);
                    await socketServer.Send(json);
                }

                MsgBox.Show(this, MsgBoxIcon.Info, MsgBoxButton.OK, MsgBoxSound.Asterisk,
                    "情報", "拡張ツールの利用を開始しました。");
            }
            else
            {
                await socketServer.Stop();
                MsgBox.Show(this, MsgBoxIcon.Info, MsgBoxButton.OK, MsgBoxSound.Asterisk,
                    "情報", "拡張ツールの利用を停止しました。");
            }
        }
        private void MenuOpenUrl_Click(object sender, EventArgs e)
        {
            if (connectData == null)
                return;
            var url = ytUtil.GetLiveUrl(connectData.Id);
            if (!Uri.IsWellFormedUriString(url, UriKind.Absolute))
                return;
            Process.Start(url);
        }
        private void MenuZoom_Click(object sender, EventArgs e)
        {
            config.Main.Zoom = (double)((ToolStripMenuItem)sender).Tag;
            config.Log.Zoom = config.Main.Zoom;
            foreach (ToolStripMenuItem item in menuZoom.DropDownItems)
            {
                item.Checked = (double)item.Tag == config.Main.Zoom ? true : false;
            }
            chromChat.SetZoom(config.Main.Zoom);
            chromPick.SetZoom(config.Main.Zoom);
        }
        private void MenuCopyLiveUrl_Click(object sender, EventArgs e)
        {
            if (connectData == null)
                return;
            Clipboard.SetText(ytUtil.GetLiveUrl(connectData.Id));
        }
        private void MenuCopyShortUrl_Click(object sender, EventArgs e)
        {
            if (connectData == null)
                return;
            Clipboard.SetText(ytUtil.GetShortUrl(connectData.Id));
        }
        private void MenuCopyImage_Click(object sender, EventArgs e)
        {
            if (connectData == null)
                return;
            Clipboard.SetText(connectData.Image);
        }
        private void MenuFont_Click(object sender, EventArgs e)
        {
            if (fontForm != null)
                if (!fontForm.IsDisposed)
                    return;
            fontForm = new FontForm(config);
            fontForm.Disposed += (c, d) =>
            {
                logChat.FontName = config.Font.Name;
                logPick.FontName = config.Font.Name;
                chromChat.SetFont(config.Font.Name);
                chromPick.SetFont(config.Font.Name);
            };
            fontForm.Show(this);
        }
        private void MenuEditWebSocket_Click(object sender, EventArgs e)
        {
            if (webSocketForm != null)
                if (!webSocketForm.IsDisposed)
                    return;
            webSocketForm = new WebScoketForm(config);
            webSocketForm.Show(this);
        }
        private void MenuClearCache_Click(object sender, EventArgs e)
        {
            MsgBox.Show(this, MsgBoxIcon.Question, MsgBoxButton.OKCancel, MsgBoxSound.Asterisk,
                "ブラウザキャッシュの削除",
                "アプリを終了しブラウザのキャッシュを削除します。再起動は手動で行って下さい。実行しますか？",
                (result) =>
                {
                    if (result != DialogResult.OK)
                        return;
                    clearCache = true;
                    Close();
                });
        }
        private void MenuSaveConfig_Click(object sender, EventArgs e)
        {
            SaveConfig();
        }
        private void MenuLoadConfig_Click(object sender, EventArgs e)
        {
            LoadConfig(false);
        }
        #endregion

        #region Menu Debug
        private void MenuPickDevTool_Click(object sender, EventArgs e)
        {
            chromPick.WebBrowser?.ShowDevTools();
        }

        private void MenuChatDevTool_Click(object sender, EventArgs e)
        {
            chromChat.WebBrowser?.ShowDevTools();
        }
        #endregion
    }
}
