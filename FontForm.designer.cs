﻿namespace Chatsumi
{
    partial class FontForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FontForm));
            this.comboFont = new System.Windows.Forms.ComboBox();
            this.panelSample = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // comboFont
            // 
            this.comboFont.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboFont.FormattingEnabled = true;
            this.comboFont.Location = new System.Drawing.Point(12, 12);
            this.comboFont.Name = "comboFont";
            this.comboFont.Size = new System.Drawing.Size(181, 20);
            this.comboFont.TabIndex = 0;
            this.comboFont.SelectedIndexChanged += new System.EventHandler(this.ComboFont_SelectedIndexChanged);
            // 
            // panelSample
            // 
            this.panelSample.Location = new System.Drawing.Point(12, 38);
            this.panelSample.Name = "panelSample";
            this.panelSample.Size = new System.Drawing.Size(181, 44);
            this.panelSample.TabIndex = 1;
            // 
            // FontForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(205, 94);
            this.Controls.Add(this.panelSample);
            this.Controls.Add(this.comboFont);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FontForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "フォントの設定";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FontForm_FormClosed);
            this.Load += new System.EventHandler(this.FontForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox comboFont;
        private System.Windows.Forms.Panel panelSample;
    }
}