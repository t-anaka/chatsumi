﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace System.Net.WebSockets
{
    public class WebSocketClient
    {
        public event EventHandler Connected;
        public event EventHandler Closed;
        public event EventHandler<WebSocketRecvEventArgs> Received;
        public event EventHandler<WebSocketErrorEventArgs> Error;

        CancellationTokenSource cancel;
        ClientWebSocket socket = null;

        public bool IsConnected => socket == null ? false : socket.State == WebSocketState.Open;

        public WebSocketClient()
        {
        }

        public async Task Send(string data)
        {
            var buff = new ArraySegment<byte>(Encoding.UTF8.GetBytes(data));
            await socket.SendAsync(buff, WebSocketMessageType.Text, true, CancellationToken.None);
        }

        private async Task SendZero()
        {
            var buff = new ArraySegment<byte>(new byte[0]);
            await socket.SendAsync(buff, WebSocketMessageType.Text, true, CancellationToken.None);
        }

        private Exception GetExeption(Exception e)
        {
            if (e.Message.Contains("innerException"))
                if (e.InnerException != null)
                    return e.InnerException;
            return e;
        }

        public void Connect(string uri = "ws://localhost:55022/")
        {
            Task.Run(async () =>
            {
                try
                {
                    await Close();

                    cancel = new CancellationTokenSource();
                    socket = new ClientWebSocket();
                    await socket.ConnectAsync(new Uri(uri), cancel.Token);

                    Connected?.Invoke(this, EventArgs.Empty);

                    while (socket.State == WebSocketState.Open)
                    {
                        var buff = new ArraySegment<byte>(new byte[1024 * 512]);
                        WebSocketReceiveResult recv;
                        try
                        {
                            recv = await socket.ReceiveAsync(buff, cancel.Token);
                        }
                        catch (OperationCanceledException)
                        {
                            break;
                        }
                        if (recv.Count == 0)
                            break;
                        var msg = Encoding.UTF8.GetString(buff.Take(recv.Count).ToArray());
                        Received?.Invoke(this, new WebSocketRecvEventArgs
                        {
                            Socket = socket,
                            Message = msg,
                        });
                    }
                }
                catch (Exception ex)
                {
                    Error?.Invoke(this, new WebSocketErrorEventArgs
                    {
                        Exception = GetExeption(ex)
                    });
                }
                finally
                {
                    try
                    {
                        await socket.CloseAsync(WebSocketCloseStatus.NormalClosure, string.Empty, CancellationToken.None);
                    }
                    catch { }

                    socket.Dispose();
                    socket = null;

                    cancel.Dispose();
                    cancel = null;

                    Closed?.Invoke(this, EventArgs.Empty);
                }
            });
        }

        public async Task Close()
        {
            if (socket == null)
                return;
            if (socket.State == WebSocketState.Closed)
                return;
            if (cancel == null)
                return;
            await SendZero();
            cancel.Cancel();
        }
    }
}
