﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading.Tasks;

namespace System.Net.WebSockets
{
    public class WebSocketAcceptEventArgs : EventArgs
    {
        public WebSocket Socket { get; set; }
    }

    public class WebSocketRecvEventArgs : EventArgs
    {
        public WebSocket Socket { get; set; }
        public string Message { get; set; }
    }

    public class WebSocketErrorEventArgs : EventArgs
    {
        public Exception Exception { get; set; }
    }
}
