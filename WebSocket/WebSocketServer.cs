﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace System.Net.WebSockets
{
    public class WebSocketServer : IDisposable
    {
        public event EventHandler Opened;
        public event EventHandler Closed;
        public event EventHandler<WebSocketAcceptEventArgs> Accepted;
        public event EventHandler<WebSocketRecvEventArgs> Received;
        public event EventHandler<WebSocketErrorEventArgs> Error;

        CancellationTokenSource cancel;
        HttpListener listener;
        List<WebSocket> sockets = new List<WebSocket>();

        public bool IsListening => listener == null ? false : listener.IsListening;

        public WebSocketServer()
        {
        }

        public async Task Send(WebSocket socket, string message)
        {
            var buff = new ArraySegment<byte>(Encoding.UTF8.GetBytes(message));
            await socket.SendAsync(buff, WebSocketMessageType.Text, true, cancel.Token);
        }

        public async Task Send(string message)
        {
            var buff = new ArraySegment<byte>(Encoding.UTF8.GetBytes(message));
            for (var i = 0; i < sockets.Count; i++)
            {
                await sockets[i].SendAsync(buff, WebSocketMessageType.Text, true, cancel.Token);
            }
        }

        private async Task SendZero()
        {
            var buff = new ArraySegment<byte>(new byte[0]);
            for (var i = 0; i < sockets.Count; i++)
            {
                await sockets[i].SendAsync(buff, WebSocketMessageType.Text, true, cancel.Token);
            }
        }

        private Exception GetExeption(Exception e)
        {
            if (e.Message.Contains("innerException"))
                if (e.InnerException != null)
                    return e.InnerException;
            return e;
        }

        private async Task Accept(HttpListenerContext context)
        {
            var accept = await context.AcceptWebSocketAsync(null);
            var socket = accept.WebSocket;

            Accepted?.Invoke(this, new WebSocketAcceptEventArgs
            {
                Socket = socket,
            });

            var task = Task.Run(async () =>
            {
                try
                {
                    sockets.Add(socket);
                    while (socket.State == WebSocketState.Open)
                    {
                        var buffer = new ArraySegment<byte>(new byte[1024 * 1024]);
                        WebSocketReceiveResult recv;
                        try
                        {
                            recv = await socket.ReceiveAsync(buffer, cancel.Token);
                        }
                        catch (OperationCanceledException)
                        {
                            break;
                        }
                        if (recv.Count == 0)
                            break;
                        var msg = Encoding.UTF8.GetString(buffer.Take(recv.Count).ToArray());
                        Received?.Invoke(this, new WebSocketRecvEventArgs
                        {
                            Socket = socket,
                            Message = msg
                        });
                    }
                }
                catch (Exception ex)
                {
                    Error?.Invoke(this, new WebSocketErrorEventArgs
                    {
                        Exception = GetExeption(ex)
                    });
                }
                finally
                {
                    sockets.Remove(socket);
                    //socket.Dispose();
                }
            });
        }

        public void Start(string uri = "http://localhost:55022/")
        {
            Task.Run(async () =>
            {
                try
                {
                    await Stop();

                    cancel = new CancellationTokenSource();

                    listener = new HttpListener();
                    listener.Prefixes.Clear();
                    listener.Prefixes.Add(uri);
                    listener.Start();

                    Opened?.Invoke(this, EventArgs.Empty);

                    while (!cancel.IsCancellationRequested)
                    {
                        HttpListenerContext context;
                        try
                        {
                            context = await listener.GetContextAsync().Cancel(cancel.Token);
                        }
                        catch (OperationCanceledException)
                        {
                            break;
                        }
                        if (context.Request.IsWebSocketRequest)
                        {
                            var task = Task.Run(() => Accept(context));
                        }
                        else
                        {
                            context.Response.StatusCode = 400;
                            context.Response.Close();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Error?.Invoke(this, new WebSocketErrorEventArgs
                    {
                        Exception = GetExeption(ex)
                    });
                }
                finally
                {
                    sockets.Clear();

                    listener.Close();
                    listener = null;

                    cancel.Dispose();
                    cancel = null;

                    Closed?.Invoke(this, EventArgs.Empty);
                }
            });
        }

        public async Task Stop()
        {
            if (listener == null)
                return;
            if (cancel == null)
                return;
            await SendZero();
            cancel.Cancel();
        }

        public void Dispose()
        {
            var task = Stop();
        }
    }
}
