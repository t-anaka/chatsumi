﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using System.Drawing;

namespace Chatsumi
{
    public class MainConfig
    {
        public Rectangle Position { get; set; } = Rectangle.Empty;
        public int Splitter { get; set; } = 0;
        public bool TopMost { get; set; } = false;
        public double Zoom { get; set; } = 1.0;
        public int UpdateInterval { get; set; } = 500;
    }

    public class PickupConfig
    {
        public bool UserName { get; set; } = true;
        public bool Owner { get; set; } = true;
        public bool Moderator { get; set; } = true;
        public bool Member { get; set; } = false;
        public bool SuperChat { get; set; } = false;
        public bool Membership { get; set; } = false;
        public bool Sticker { get; set; } = false;
        public PickupNameConfig PickupName { get; set; } = new PickupNameConfig();
        public bool AutoOpen { get; set; } = true;
    }

    public class PickupNameConfig
    {
        public Rectangle Position { get; set; } = Rectangle.Empty;
        public List<string> Names { get; set; } = new List<string>();
    }

    public class NGWordConfig
    {
        public Rectangle Position { get; set; } = Rectangle.Empty;
        public List<string> Words { get; set; } = new List<string>();
    }

    public class VoiceConfig
    {
        public Point Position { get; set; } = Point.Empty;
        public string Name { get; set; } = "";
        public int Rate { get; set; } = 2;
        public int Volume { get; set; } = 50;
        public int Count { get; set; } = 3;
    }

    public class BouyomiConfig
    {
        public Point Posision { get; set; } = Point.Empty;
        public string ServerPath { get; set; } = "";
        public int VoiceType { get; set; } = 0;
        public int Speed { get; set; } = -1;
        public int Tone { get; set; } = -1;
        public int Volume { get; set; } = -1;
        public int Count { get; set; } = -1;
    }

    public enum SpeechMode
    {
        Off, Pick, All
    }

    public class SpeechConfig
    {
        public SpeechMode Mode { get; set; } = SpeechMode.Off;
        public NGWordConfig NGWord { get; set; } = new NGWordConfig();
        public VoiceConfig Voice { get; set; } = new VoiceConfig();
        public bool UseBouyomi { get; set; } = false;
        public BouyomiConfig Bouyomi { get; set; } = new BouyomiConfig();
    }

    public class LogConfig
    {
        public Rectangle Position { get; set; } = Rectangle.Empty;
        public bool Enable { get; set; } = true;
        public double Zoom = 1.0;
        public int Limit { get; set; } = 3000;
        public int TitleLen { get; set; } = 25;
    }

    public class FontConfig
    {
        public Point Position { get; set; } = Point.Empty;
        public string Name { get; set; } = "";
    }

    public class WebSocketConfig
    {
        public Point Position { get; set; } = Point.Empty;
        public string Host { get; set; } = "localhost";
        public int Port { get; set; } = 55022;
    }

    public class DevToolConfig
    {
        public bool Enable { get; set; } = false;
    }

    public class Config
    {
        public string Ver { get; set; } = "";
        public MainConfig Main { get; set; } = new MainConfig();
        public PickupConfig Pickup { get; set; } = new PickupConfig();
        public SpeechConfig Speech { get; set; } = new SpeechConfig();
        public LogConfig Log { get; set; } = new LogConfig();
        public FontConfig Font { get; set; } = new FontConfig();
        public WebSocketConfig WebSocket { get; set; } = new WebSocketConfig();
        public DevToolConfig DevTool { get; set; } = new DevToolConfig();
    }
}
