﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;

namespace Chatsumi
{
    public enum MsgBoxIcon
    {
        App, Caution, Info, Question,
    }

    public enum MsgBoxButton
    {
        OK, OKCancel,
    }

    public enum MsgBoxSound
    {
        None, Asterisk, Beep, Exclamation, Hand, Question,
    }

    public partial class MsgBox : Form
    {
        public string Message
        {
            get => textText.Text;
            set => textText.Text = value;
        }

        MsgBoxSound sound = MsgBoxSound.None;

        public MsgBox()
        {
            InitializeComponent();
        }

        private void MsgBox_Load(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Shown += (a, b) =>
            {
                switch (sound)
                {
                    case MsgBoxSound.Asterisk:
                        SystemSounds.Asterisk.Play();
                        break;
                    case MsgBoxSound.Beep:
                        SystemSounds.Beep.Play();
                        break;
                    case MsgBoxSound.Exclamation:
                        SystemSounds.Exclamation.Play();
                        break;
                    case MsgBoxSound.Hand:
                        SystemSounds.Hand.Play();
                        break;
                    case MsgBoxSound.Question:
                        SystemSounds.Question.Play();
                        break;
                }
            };
        }

        private void MsgBox_FormClosed(object sender, FormClosedEventArgs e)
        {
            Icon.Dispose();
        }

        public void SetIcon(MsgBoxIcon icon)
        {
            switch (icon)
            {
                case MsgBoxIcon.Caution:
                    Icon = Properties.Resources.ico_caution;
                    break;
                case MsgBoxIcon.Info:
                    Icon = Properties.Resources.ico_info;
                    break;
                case MsgBoxIcon.Question:
                    Icon = Properties.Resources.ico_question;
                    break;
                case MsgBoxIcon.App:
                    Icon = Properties.Resources.ico_app;
                    break;
            }
        }

        public void SetButton(MsgBoxButton button)
        {
            switch (button)
            {
                case MsgBoxButton.OK:
                    buttonLeft.Visible = false;
                    buttonRight.Text = "OK";
                    break;
                case MsgBoxButton.OKCancel:
                    buttonLeft.Text = "OK";
                    buttonRight.Text = "Cancel";
                    break;
            }
        }

        public void SetSound(MsgBoxSound sound) => this.sound = sound;

        public static void Show(Form parent, MsgBoxIcon icon, MsgBoxButton button, MsgBoxSound sound, string title, string message, Action<DialogResult> closed = null)
        {
            parent.Invoke((MethodInvoker)(() =>
            {
                var box = new MsgBox();
                box.DialogResult = DialogResult.Cancel;
                box.SetIcon(icon);
                box.SetButton(button);
                box.SetSound(sound);
                box.Text = title;
                box.Message = message;
                if (closed != null)
                    box.Disposed += (a, b) => closed(box.DialogResult);
                box.Show(parent);
                box.Location = GetCenter(parent, box);
            }));
        }

        public static Point GetCenter(Form parent, MsgBox box)
        {
            var x = parent.Width / 2 - box.Width / 2;
            var y = Math.Min(100, parent.Height / 2 - box.Height / 2);
            return parent.Location + new Size(x, y);
        }

        private void ButtonLeft_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void ButtonRight_Click(object sender, EventArgs e)
        {
            if (buttonRight.Text == "OK")
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                DialogResult = DialogResult.Cancel;
            }
            Close();
        }
    }
}
