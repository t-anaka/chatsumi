﻿namespace Chatsumi
{
    partial class NGWordForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NGWordForm));
            this.editWord = new Chatsumi.ListEditor();
            this.SuspendLayout();
            // 
            // editWord
            // 
            this.editWord.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editWord.InputToolTipText = "読み上げNGワードを入力";
            this.editWord.Location = new System.Drawing.Point(0, 0);
            this.editWord.Name = "editWord";
            this.editWord.Size = new System.Drawing.Size(313, 390);
            this.editWord.TabIndex = 0;
            this.editWord.ItemsChanged += new System.EventHandler(this.EditWord_ItemsChanged);
            // 
            // NGWordForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(313, 390);
            this.Controls.Add(this.editWord);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "NGWordForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "NGワードの設定";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form_FormClosed);
            this.ResumeLayout(false);

        }

        #endregion

        private ListEditor editWord;
    }
}