﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Chatsumi
{
    public partial class VoiceForm : Form
    {
        VoiceConfig config;
        Speech speech;

        public VoiceForm(VoiceConfig config, Speech speech)
        {
            this.config = config;
            this.speech = speech;
            InitializeComponent();

            foreach (var name in speech.Installed)
            {
                comboVoice.Items.Add(name);
            }
            if (comboVoice.Items.Contains(config.Name))
            {
                comboVoice.SelectedItem = config.Name;
            }
            else
            {
                comboVoice.SelectedIndex = 0;
            }

            labelRate.Text = $"{config.Rate}";
            trackRate.Value = config.Rate;

            labelVolume.Text = $"{config.Volume}";
            trackVolume.Value = config.Volume;

            labelSpeechCount.Text = $"{config.Count}";
            trackCount.Value = config.Count;
        }

        public void Show(Form parent)
        {
            Show((IWin32Window)parent);

            if (config.Position.IsEmpty)
            {
                var x = parent.Width / 2 - Width / 2;
                var y = Math.Min(100, parent.Height / 2 - Height / 2);
                Location = parent.Location + new Size(x, y);
            }
            else
            {
                Location = config.Position;
            }
        }

        private void comboVoice_SelectedIndexChanged(object sender, EventArgs e)
        {
            var i = comboVoice.SelectedIndex;
            var name = (string)comboVoice.SelectedItem;

            Exception ex;
            if (speech.TestSelectVoice(name, out ex))
            {
                config.Name = name;
            }
            else
            {
                MsgBox.Show(this, MsgBoxIcon.Caution, MsgBoxButton.OK, MsgBoxSound.Asterisk, "エラー", ex.Message);
                comboVoice.SelectedItem = speech.DefaultVoice;
            }
        }

        private void TrackRate_Scroll(object sender, EventArgs e)
        {
            labelRate.Text = $"{trackRate.Value}";
            config.Rate = trackRate.Value;
        }

        private void TrackVolume_Scroll(object sender, EventArgs e)
        {
            labelVolume.Text = $"{trackVolume.Value}";
            config.Volume = trackVolume.Value;
        }

        private void TrackSpeechCount_Scroll(object sender, EventArgs e)
        {
            labelSpeechCount.Text = $"{trackCount.Value}";
            config.Count = trackCount.Value;
        }

        private void buttonTest_Click(object sender, EventArgs e)
        {
            speech.Speak("読み上げテストです");
        }

        private void EditVoiceForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            config.Position = Location;
        }
    }
}
