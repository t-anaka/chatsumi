﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FNF.Utility;
using System.Diagnostics;

namespace Chatsumi
{
    public class Bouyomi : IDisposable
    {
        private BouyomiChanClient client = new BouyomiChanClient();
        private Random random = new Random();

        public BouyomiConfig Config { get; set; }

        public bool StartServer()
        {
            if (Config.ServerPath == null)
                return false;

            if (!File.Exists(Config.ServerPath))
                return false;

            var name = Path.GetFileNameWithoutExtension(Config.ServerPath);
            var procs = Process.GetProcessesByName(name);
            if (procs.Length > 0)
                return true;

            var proc = Process.Start(Config.ServerPath);
            proc.WaitForInputIdle();

            return true;
        }

        public bool Speak(string text)
        {
            if (text.Length == 0)
                return true;
            try
            {
                if (Config.Count > 0)
                {
                    if (client.TalkTaskCount >= Config.Count)
                        return true;
                }
                var voice = Config.VoiceType == 99 ? random.Next(1, 8) : Config.VoiceType;
                client.AddTalkTask(text, Config.Speed, Config.Tone, Config.Volume, (VoiceType)voice);
            }
            catch
            {
                var name = Path.GetFileNameWithoutExtension(Config.ServerPath);
                return ProcUtil.Exist(name);
            }
            return true;
        }

        public void Dispose()
        {
            client.Dispose();
        }
    }
}
