﻿namespace Chatsumi
{
    partial class ListEditor
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.listEdit = new System.Windows.Forms.ListBox();
            this.toolMain = new FlatToolStrip();
            this.textInput = new ToolStripSpringTextBox();
            this.buttonAdd = new System.Windows.Forms.ToolStripButton();
            this.buttonDel = new System.Windows.Forms.ToolStripButton();
            this.buttonUndo = new System.Windows.Forms.ToolStripButton();
            this.buttonUp = new System.Windows.Forms.ToolStripButton();
            this.buttonDown = new System.Windows.Forms.ToolStripButton();
            this.toolMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // listEdit
            // 
            this.listEdit.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listEdit.FormattingEnabled = true;
            this.listEdit.ItemHeight = 12;
            this.listEdit.Location = new System.Drawing.Point(0, 25);
            this.listEdit.Name = "listEdit";
            this.listEdit.Size = new System.Drawing.Size(366, 330);
            this.listEdit.TabIndex = 3;
            this.listEdit.SelectedIndexChanged += new System.EventHandler(this.ListEdit_SelectedIndexChanged);
            // 
            // toolMain
            // 
            this.toolMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.textInput,
            this.buttonAdd,
            this.buttonDel,
            this.buttonUndo,
            this.buttonUp,
            this.buttonDown});
            this.toolMain.Location = new System.Drawing.Point(0, 0);
            this.toolMain.Name = "toolMain";
            this.toolMain.Size = new System.Drawing.Size(366, 25);
            this.toolMain.TabIndex = 2;
            this.toolMain.Text = "flatToolStrip1";
            // 
            // textInput
            // 
            this.textInput.Name = "textInput";
            this.textInput.Size = new System.Drawing.Size(217, 25);
            this.textInput.ToolTipText = "入力ワード";
            this.textInput.EnterKeyDown += new System.Windows.Forms.KeyEventHandler(this.TextInput_EnterKeyDown);
            // 
            // buttonAdd
            // 
            this.buttonAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonAdd.Image = global::Chatsumi.Properties.Resources.png_plus_512;
            this.buttonAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(23, 22);
            this.buttonAdd.Text = "追加";
            this.buttonAdd.Click += new System.EventHandler(this.ButtonAdd_Click);
            // 
            // buttonDel
            // 
            this.buttonDel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonDel.Image = global::Chatsumi.Properties.Resources.png_minus_512;
            this.buttonDel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonDel.Name = "buttonDel";
            this.buttonDel.Size = new System.Drawing.Size(23, 22);
            this.buttonDel.Text = "削除";
            this.buttonDel.Click += new System.EventHandler(this.ButtonDel_Click);
            // 
            // buttonUndo
            // 
            this.buttonUndo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonUndo.Image = global::Chatsumi.Properties.Resources.png_undo_512;
            this.buttonUndo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonUndo.Name = "buttonUndo";
            this.buttonUndo.Size = new System.Drawing.Size(23, 22);
            this.buttonUndo.Text = "戻す";
            this.buttonUndo.Click += new System.EventHandler(this.ButtonUndo_Click);
            // 
            // buttonUp
            // 
            this.buttonUp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonUp.Image = global::Chatsumi.Properties.Resources.png_up_512;
            this.buttonUp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonUp.Name = "buttonUp";
            this.buttonUp.Size = new System.Drawing.Size(23, 22);
            this.buttonUp.Text = "上に移動";
            this.buttonUp.Click += new System.EventHandler(this.ButtonUp_Click);
            // 
            // buttonDown
            // 
            this.buttonDown.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonDown.Image = global::Chatsumi.Properties.Resources.png_down_512;
            this.buttonDown.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonDown.Name = "buttonDown";
            this.buttonDown.Size = new System.Drawing.Size(23, 22);
            this.buttonDown.Text = "下に移動";
            this.buttonDown.Click += new System.EventHandler(this.ButtonDown_Click);
            // 
            // ListEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.listEdit);
            this.Controls.Add(this.toolMain);
            this.Name = "ListEditor";
            this.Size = new System.Drawing.Size(366, 355);
            this.toolMain.ResumeLayout(false);
            this.toolMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listEdit;
        private FlatToolStrip toolMain;
        private ToolStripSpringTextBox textInput;
        private System.Windows.Forms.ToolStripButton buttonAdd;
        private System.Windows.Forms.ToolStripButton buttonDel;
        private System.Windows.Forms.ToolStripButton buttonUndo;
        private System.Windows.Forms.ToolStripButton buttonUp;
        private System.Windows.Forms.ToolStripButton buttonDown;
    }
}
