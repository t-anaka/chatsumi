﻿namespace Chatsumi
{
    partial class VoiceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VoiceForm));
            this.label1 = new System.Windows.Forms.Label();
            this.comboVoice = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.trackRate = new System.Windows.Forms.TrackBar();
            this.label3 = new System.Windows.Forms.Label();
            this.trackVolume = new System.Windows.Forms.TrackBar();
            this.buttonTest = new System.Windows.Forms.Button();
            this.trackCount = new System.Windows.Forms.TrackBar();
            this.label4 = new System.Windows.Forms.Label();
            this.labelRate = new System.Windows.Forms.Label();
            this.labelVolume = new System.Windows.Forms.Label();
            this.labelSpeechCount = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.trackRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackVolume)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackCount)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "音声";
            // 
            // comboVoice
            // 
            this.comboVoice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboVoice.FormattingEnabled = true;
            this.comboVoice.Location = new System.Drawing.Point(47, 12);
            this.comboVoice.Name = "comboVoice";
            this.comboVoice.Size = new System.Drawing.Size(217, 20);
            this.comboVoice.TabIndex = 1;
            this.comboVoice.SelectedIndexChanged += new System.EventHandler(this.comboVoice_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "速度";
            // 
            // trackRate
            // 
            this.trackRate.Location = new System.Drawing.Point(47, 52);
            this.trackRate.Minimum = -10;
            this.trackRate.Name = "trackRate";
            this.trackRate.Size = new System.Drawing.Size(202, 45);
            this.trackRate.TabIndex = 3;
            this.trackRate.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackRate.Scroll += new System.EventHandler(this.TrackRate_Scroll);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "音量";
            // 
            // trackVolume
            // 
            this.trackVolume.Location = new System.Drawing.Point(47, 93);
            this.trackVolume.Maximum = 100;
            this.trackVolume.Name = "trackVolume";
            this.trackVolume.Size = new System.Drawing.Size(202, 45);
            this.trackVolume.TabIndex = 5;
            this.trackVolume.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackVolume.Value = 50;
            this.trackVolume.Scroll += new System.EventHandler(this.TrackVolume_Scroll);
            // 
            // buttonTest
            // 
            this.buttonTest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonTest.Location = new System.Drawing.Point(197, 172);
            this.buttonTest.Name = "buttonTest";
            this.buttonTest.Size = new System.Drawing.Size(75, 23);
            this.buttonTest.TabIndex = 6;
            this.buttonTest.Text = "テスト";
            this.buttonTest.UseVisualStyleBackColor = true;
            this.buttonTest.Click += new System.EventHandler(this.buttonTest_Click);
            // 
            // trackCount
            // 
            this.trackCount.Location = new System.Drawing.Point(47, 134);
            this.trackCount.Minimum = 1;
            this.trackCount.Name = "trackCount";
            this.trackCount.Size = new System.Drawing.Size(202, 45);
            this.trackCount.TabIndex = 8;
            this.trackCount.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackCount.Value = 2;
            this.trackCount.Scroll += new System.EventHandler(this.TrackSpeechCount_Scroll);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 138);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 7;
            this.label4.Text = "制限";
            // 
            // labelRate
            // 
            this.labelRate.AutoSize = true;
            this.labelRate.Location = new System.Drawing.Point(253, 54);
            this.labelRate.Name = "labelRate";
            this.labelRate.Size = new System.Drawing.Size(11, 12);
            this.labelRate.TabIndex = 9;
            this.labelRate.Text = "0";
            // 
            // labelVolume
            // 
            this.labelVolume.AutoSize = true;
            this.labelVolume.Location = new System.Drawing.Point(253, 95);
            this.labelVolume.Name = "labelVolume";
            this.labelVolume.Size = new System.Drawing.Size(11, 12);
            this.labelVolume.TabIndex = 10;
            this.labelVolume.Text = "0";
            // 
            // labelSpeechCount
            // 
            this.labelSpeechCount.AutoSize = true;
            this.labelSpeechCount.Location = new System.Drawing.Point(253, 136);
            this.labelSpeechCount.Name = "labelSpeechCount";
            this.labelSpeechCount.Size = new System.Drawing.Size(11, 12);
            this.labelSpeechCount.TabIndex = 11;
            this.labelSpeechCount.Text = "0";
            // 
            // VoiceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 207);
            this.Controls.Add(this.labelSpeechCount);
            this.Controls.Add(this.labelVolume);
            this.Controls.Add(this.labelRate);
            this.Controls.Add(this.buttonTest);
            this.Controls.Add(this.trackCount);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.trackVolume);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.trackRate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboVoice);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VoiceForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "音声の設定";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.EditVoiceForm_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.trackRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackVolume)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackCount)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboVoice;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TrackBar trackRate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TrackBar trackVolume;
        private System.Windows.Forms.Button buttonTest;
        private System.Windows.Forms.TrackBar trackCount;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelRate;
        private System.Windows.Forms.Label labelVolume;
        private System.Windows.Forms.Label labelSpeechCount;
    }
}