﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO.Compression;
using System.Threading;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Collections.Concurrent;
using CefSharp;

namespace Chatsumi
{
    public class LogWriter : IDisposable
    {
        static object locked = new object();
        Chromium chromChat;
        int logCount = 0;

        public string Title { get; private set; } = "";
        public string ShortTitle { get; private set; } = "";
        public int Limit { get; set; } = 3000;
        public int TitleLen { get; set; } = 25;
        public string FontName { get; set; } = "";
        public string FileName { get; private set; } = "";
        public string LastId { get; private set; } = "";
        public string LiveId { get; private set; } = "";
        public string LogDir { get; set; } = "log";
        public string TempDir { get; set; } = "!temp";
        public List<CommentData> Comments { get; private set; } = new List<CommentData>();

        public async Task Open(Chromium chat, string title, string liveId, string fname)
        {
            Write();

            logCount = 0;

            Comments.Clear();

            chromChat = chat;
            Title = title;
            FileName = fname;
            LiveId = liveId;
            ShortTitle = EditTitle(title);

            var path = Path.Combine(LogDir, ShortTitle);
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            await Read();
        }

        private string EditTitle(string title)
        {
            //emoji
            const string q = "___QUESTION___";
            var utf8 = Encoding.UTF8;
            var sjis = Encoding.GetEncoding("shift_jis");
            title = title.Replace("?", q);
            title = title.Replace("？", q);
            var bytes = Encoding.Convert(utf8, sjis, utf8.GetBytes(title));
            title = sjis.GetString(bytes);
            title = title.Replace("?", "");
            title = title.Replace(q, "？");

            title = title.Replace("\\", "￥");
            title = title.Replace("/", "／");
            title = title.Replace(":", "：");
            title = title.Replace("*", "＊");
            title = title.Replace("\"", "”");
            title = title.Replace("<", "＜");
            title = title.Replace(">", "＞");
            title = title.Replace("|", "｜");
            title = title.Replace("#", "＃");

            if (title.Length > TitleLen)
            {
                title = title.Substring(0, TitleLen);
                title = title.Trim(new char[] { ' ', '　', '_', '＿' });
            }
            else
            {
                title = title.Trim(new char[] { ' ', '　', '_', '＿' });
            }

            var tmpTile = title;
            for (var i = 0; i < 10000; i++)
            {
                if (i > 0)
                {
                    title = $"{tmpTile}[{i}]";
                }
                var path = Path.Combine(LogDir, title);
                if (Directory.Exists(path))
                {
                    var liveId = "";
                    foreach (var file in Directory.GetFiles(path, $"*.html"))
                    {
                        var html = File.ReadAllText(file);
                        var doc = new HtmlDocument();
                        doc.LoadHtml(html);
                        var node = doc.DocumentNode.SelectSingleNode("//title");
                        if (node == null)
                            break;
                        liveId = node.GetAttributeValue("live-id", "");
                        if (liveId != "")
                            break;
                    }
                    if (liveId == "")
                        break;
                    if (LiveId != liveId)
                        continue;
                }
                break;
            }
            return title;
        }

        public void Append(CommentData data)
        {
            lock (locked)
            {
                if (LastId == data.Id)
                    return;
                if (Comments.Count >= Limit)
                {
                    Write();
                }
                Comments.Add(data);
                LastId = data.Id;
            }
        }

        public async Task Read()
        {
            try
            {
                CommentData[] comments = null;
                var sb = new StringBuilder();
                lock (locked)
                {
                    var path = Path.Combine(LogDir, ShortTitle);
                    var files = Directory.GetFiles(path, $"{FileName}??.html");
                    if (files.Length == 0)
                        return;
                    var read = false;
                    for (var i = 0; i < files.Length; i++)
                    {
                        var html = File.ReadAllText(files[i]);
                        var doc = new HtmlDocument();
                        doc.LoadHtml(html);

                        var body = doc.DocumentNode.SelectSingleNode("//body");
                        var count = int.Parse(body.GetAttributeValue("count", "0"));

                        if (!read)
                        {
                            read = count < Limit;
                        }
                        if (!read)
                        {
                            logCount++;
                            continue;
                        }

                        var innerHtml = body.InnerHtml.Trim('\n');
                        var itemsHtml = chromChat.ReplaceScript(innerHtml);

                        sb.Append(itemsHtml);

                        try { File.Delete(files[i]); } catch { }
                    }
                    if (sb.Length == 0)
                        return;
                }
                var data = (string)await chromChat.EvaluateScriptAsync($"getChat(\"{sb.ToString()}\")");
                comments = JsonConvert.DeserializeObject<CommentData[]>(data);
                foreach (var comment in comments)
                    Append(comment);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                FileUtil.Error("error/err.txt", ex.ToString());
            }
        }

        private void Write()
        {
            if (string.IsNullOrEmpty(ShortTitle))
                return;
            Write(Path.Combine(LogDir, ShortTitle));
            Comments.Clear();
            logCount++;
        }

        private void Write(string dir)
        {
            try
            {
                if (Comments.Count == 0)
                    return;

                var html = File.ReadAllText("html/log.html");
                var doc = new HtmlDocument();
                doc.LoadHtml(html);

                var titleNode = doc.DocumentNode.SelectSingleNode("//title");
                titleNode.InnerHtml = Title;
                titleNode.SetAttributeValue("live-id", LiveId);

                var bodyNode = doc.DocumentNode.SelectSingleNode("//body");
                if (FontName != "")
                    bodyNode.Attributes.Add("style", $"font-family: {FontName}");

                var innerHtml = string.Join("\n", Comments.Select(a => a.Html));
                bodyNode.InnerHtml = $"\n{innerHtml}\n";
                bodyNode.SetAttributeValue("count", Comments.Count.ToString());

                var styleFile = Path.Combine(LogDir, ShortTitle, "style.css");
                if (File.Exists("html/style.css"))
                    File.Copy("html/style.css", styleFile, true);
                else
                    File.Copy("html/default.css", styleFile, true);

                var fname = $"{FileName}{logCount.ToString("00")}.html";
                var path = Path.Combine(dir, fname);
                File.WriteAllText(path, doc.DocumentNode.OuterHtml);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                FileUtil.Error("error/err.txt", ex.ToString());
            }
        }

        public string[] CreateTemp()
        {
            var list = new List<string>();
            try
            {
                lock (locked)
                {
                    if (string.IsNullOrEmpty(ShortTitle))
                        return new string[0];

                    var tempPath = "";
                    var path = Path.Combine(LogDir, ShortTitle);
                    tempPath = Path.Combine(path, TempDir);
                    if (Directory.Exists(tempPath))
                        Directory.Delete(tempPath, true);

                    Directory.CreateDirectory(tempPath);

                    var styleFile = Path.Combine(tempPath, "style.css");
                    if (File.Exists("html/style.css"))
                        File.Copy("html/style.css", styleFile, true);
                    else
                        File.Copy("html/default.css", styleFile, true);

                    foreach (var file in Directory.GetFiles(path, $"{FileName}??.html"))
                    {
                        var copyPath = Path.Combine(tempPath, $"{Path.GetFileName(file)}");
                        File.Copy(file, copyPath);
                    }

                    Write(tempPath);
                    list.AddRange(Directory.GetFiles(tempPath, $"{FileName}??.html"));
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                FileUtil.Error("error/err.txt", ex.ToString());
            }
            return list.ToArray();
        }

        public void DeleteTemp()
        {
            try
            {
                lock (locked)
                {
                    var tempPath = Path.Combine(LogDir, ShortTitle, TempDir);
                    if (Directory.Exists(tempPath))
                        Directory.Delete(tempPath, true);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                FileUtil.Error("error/err.txt", ex.ToString());
            }
        }

        public async Task<string[]> Zip()
        {
            var result = await Task.Run(() =>
            {
                var list = new List<string>();
                var dirs = Directory.GetDirectories(LogDir).ToList();
                if (dirs.Count == 0)
                    return new string[0];
                if (!string.IsNullOrEmpty(ShortTitle))
                {
                    var fname = Path.Combine(LogDir, ShortTitle);
                    dirs.Remove(fname);
                }
                if (dirs.Count == 0)
                    return new string[0];
                dirs.Sort();
                foreach (var dir in dirs)
                {
                    try
                    {
                        var zipName = $"{Path.GetFileName(dir)}.zip";
                        var zipPath = Path.Combine(LogDir, zipName);
                        for (var i = 0; i < 10000; i++)
                        {
                            if (i > 0)
                            {
                                zipName = $"{Path.GetFileName(dir)}[{i}].zip";
                                zipPath = Path.Combine(LogDir, zipName);
                            }
                            if (File.Exists(zipPath))
                                continue;
                            break;
                        }
                        ZipFile.CreateFromDirectory(dir, zipPath, CompressionLevel.Optimal, false);
                        try { Directory.Delete(dir, true); } catch { };
                        list.Add(zipName);
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.ToString());
                        FileUtil.Error("error/err.txt", ex.ToString());
                    }
                }
                return list.ToArray();
            });
            return result;
        }

        public void Dispose()
        {
            Write();
            DeleteTemp();
        }
    }
}
