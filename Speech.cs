﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Speech.AudioFormat;
using System.Speech.Synthesis;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Chatsumi
{
    public class Speech : IDisposable
    {
        private class SpeechInfo
        {
            public SpeechSynthesizer Speech { get; set; }
            public bool Ready { get; set; } = true;
            public string Path { get; set; } = "";
            public string Name { get; set; } = "";
            public int Id { get; set; } = -1;
            public DateTime Time { get; set; } = DateTime.MinValue;
        }

        private SpeechSynthesizer test = new SpeechSynthesizer();
        private SoundPlayer sound = new SoundPlayer();
        private List<SpeechInfo> infos = new List<SpeechInfo>();
        private int maxCount = 10;
        private string tempPath = "";
        private Timer timer = new Timer();

        public List<string> Installed { get; set; }
        public string DefaultVoice => test.Voice.Name;
        public VoiceConfig Config { get; set; }

        public Speech()
        {

        }

        public void Init(Form parent)
        {
            timer.Interval = 10000;
            timer.Tick += Timer_Tick;

            if (!Directory.Exists("speech"))
                Directory.CreateDirectory("speech");

            tempPath = Path.Combine("speech", Path.GetRandomFileName().Replace(".", ""));

            if (!Directory.Exists(tempPath))
                Directory.CreateDirectory(tempPath);

            Installed = new List<string>();

            foreach (var speech in test.GetInstalledVoices())
                Installed.Add(speech.VoiceInfo.Name);

            for (var i = 0; i < maxCount; i++)
            {
                var speech = new SpeechSynthesizer();
                speech.SpeakCompleted += Speech_SpeakCompleted;
                var info = new SpeechInfo
                {
                    Speech = speech,
                };
                infos.Add(info);
            }

            sound.Init(parent);
            sound.PlayEnd += Sound_PlayEnd;

            timer.Start();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (infos.Count == 0)
                return;
            var now = DateTime.Now;
            infos.ForEach(info =>
            {
                if (!info.Ready && now - info.Time >= TimeSpan.FromSeconds(20))
                {
                    info.Speech.SetOutputToNull();
                    sound.Close(info.Name);
                    try { File.Delete(info.Path); } catch { }
                    info.Ready = true;
                }
            });
        }

        private void Speech_SpeakCompleted(object sender, SpeakCompletedEventArgs e)
        {
            var info = infos.Find(a => a.Speech == sender);
            if (info == null)
                return;
            info.Speech.SetOutputToNull();
            info.Id = sound.Open(info.Path, info.Name);
            sound.Play(info.Name);
        }

        private void Sound_PlayEnd(int id)
        {
            var info = infos.Find(a => a.Id == id);
            sound.Close(info.Name);
            try { File.Delete(info.Path); } catch { }
            info.Ready = true;
        }

        public bool TestSelectVoice(string name)
        {
            Exception exception;
            return TestSelectVoice(name, out exception);
        }

        public bool TestSelectVoice(string name, out Exception exception)
        {
            exception = null;
            try
            {
                test.SelectVoice(name);
            }
            catch (Exception ex)
            {
                exception = ex;
                return false;
            }
            return true;
        }

        public void Speak(string text)
        {
            var runCount = infos.Count(a => !a.Ready);
            if (runCount >= Config.Count)
                return;

            var info = infos.Find(a => a.Ready);
            if (info == null)
                return;
            info.Ready = false;

            var fmt = new SpeechAudioFormatInfo(16000, AudioBitsPerSample.Sixteen, AudioChannel.Mono);
            info.Name = Path.GetRandomFileName().Replace(".", "");
            info.Path = Path.Combine(tempPath, $"{info.Name}.wav");
            info.Time = DateTime.Now;
            try
            {
                if (info.Speech.Voice.Name != Config.Name && Config.Name != "")
                    info.Speech.SelectVoice(Config.Name);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            info.Speech.Rate = Config.Rate;
            info.Speech.Volume = Config.Volume;
            info.Speech.SetOutputToWaveFile(info.Path, fmt);
            info.Speech.SpeakAsync(text);
        }

        public void Dispose()
        {
            timer.Stop();

            infos.ForEach(a =>
            {
                a.Speech.SpeakAsyncCancelAll();
                a.Speech.Dispose();
            });

            infos.Clear();
            sound.Dispose();

            try
            {
                Directory.Delete(tempPath);
            }
            catch { }

            try
            {
                Directory.Delete("speech", true);
            }
            catch { }
        }
    }
}
