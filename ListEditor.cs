﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Chatsumi
{
    public partial class ListEditor : UserControl
    {
        public event EventHandler ItemsChanged;

        public ListBox.ObjectCollection Items => listEdit.Items;
        public string InputToolTipText
        {
            get => textInput.ToolTipText;
            set => textInput.ToolTipText = value;
        }

        List<int> undoIndex = new List<int>();
        List<string> undoItem = new List<string>();

        public ListEditor()
        {
            InitializeComponent();
        }

        private void TextInput_EnterKeyDown(object sender, KeyEventArgs e)
        {
            ButtonAdd_Click(null, EventArgs.Empty);
        }

        private void ButtonAdd_Click(object sender, EventArgs e)
        {
            if (textInput.Text == "")
                return;

            var name = textInput.Text;
            var check = listEdit.Items.Cast<string>().ToArray();
            if (check.Contains(name))
                return;

            textInput.Text = "";

            listEdit.Items.Add(name);

            undoIndex.Clear();
            undoItem.Clear();

            ItemsChanged?.Invoke(listEdit, EventArgs.Empty);
        }

        private void ButtonDel_Click(object sender, EventArgs e)
        {
            if (textInput.Text == "")
                return;

            var name = textInput.Text;
            textInput.Text = "";

            var index = listEdit.SelectedIndex;
            if (index == -1)
                return;

            undoIndex.Insert(0, index);
            undoItem.Insert(0, name);

            listEdit.Items.Remove(name);
            listEdit.SelectedIndex = Math.Min(index, listEdit.Items.Count - 1);

            ItemsChanged?.Invoke(listEdit, EventArgs.Empty);
        }

        private void ButtonUndo_Click(object sender, EventArgs e)
        {
            if (undoIndex.Count == 0)
                return;

            var index = undoIndex[0];
            var name = undoItem[0];

            listEdit.Items.Insert(index, name);
            listEdit.SelectedIndex = index;

            undoIndex.RemoveAt(0);
            undoItem.RemoveAt(0);

            ItemsChanged?.Invoke(listEdit, EventArgs.Empty);
        }

        private void ListEdit_SelectedIndexChanged(object sender, EventArgs e)
        {
            textInput.Text = (string)listEdit.SelectedItem;
        }

        private void ButtonUp_Click(object sender, EventArgs e)
        {
            var index = listEdit.SelectedIndex;
            if (index < 0)
                return;

            var item = (string)listEdit.SelectedItem;
            listEdit.Items.RemoveAt(index);

            index = Math.Max(0, index - 1);
            listEdit.Items.Insert(index, item);
            listEdit.SelectedIndex = index;

            ItemsChanged?.Invoke(listEdit, EventArgs.Empty);
        }

        private void ButtonDown_Click(object sender, EventArgs e)
        {
            var index = listEdit.SelectedIndex;
            if (index < 0)
                return;

            var item = (string)listEdit.SelectedItem;
            listEdit.Items.RemoveAt(index);

            if (index >= listEdit.Items.Count)
            {
                listEdit.Items.Add(item);
                listEdit.SelectedIndex = listEdit.Items.Count - 1;
            }
            else
            {
                index++;
                listEdit.Items.Insert(index, item);
                listEdit.SelectedIndex = index;
            }

            ItemsChanged?.Invoke(listEdit, EventArgs.Empty);
        }
    }
}
