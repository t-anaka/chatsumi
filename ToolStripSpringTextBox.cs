﻿using System;
using System.Drawing;
using System.Windows.Forms;

public class ToolStripSpringTextBox : ToolStripTextBox
{
    public event KeyEventHandler EnterKeyDown;
    public event EventHandler<string> DragDropText;

    public ToolStripSpringTextBox()
    {
        AllowDrop = true;
        DragEnter += ToolStripSpringTextBox_DragEnter;
        DragDrop += ToolStripSpringTextBox_DragDrop;
    }

    private void ToolStripSpringTextBox_DragEnter(object sender, DragEventArgs e)
    {
        if (e.Data.GetDataPresent(DataFormats.Text))
            e.Effect = DragDropEffects.Copy;
        else
            e.Effect = DragDropEffects.None;
    }

    private void ToolStripSpringTextBox_DragDrop(object sender, DragEventArgs e)
    {
        var text = (string)e.Data.GetData(DataFormats.Text, false);
        Text = text;
        DragDropText?.Invoke(sender, text);
    }

    public override Size GetPreferredSize(Size constrainingSize)
    {
        if (IsOnOverflow || Owner.Orientation == Orientation.Vertical)
        {
            return DefaultSize;
        }

        Int32 width = Owner.DisplayRectangle.Width;

        if (Owner.OverflowButton.Visible)
        {
            width = width - Owner.OverflowButton.Width -
                Owner.OverflowButton.Margin.Horizontal;
        }

        Int32 springBoxCount = 0;

        foreach (ToolStripItem item in Owner.Items)
        {
            if (item.IsOnOverflow) continue;

            if (item is ToolStripSpringTextBox)
            {
                springBoxCount++;
                width -= item.Margin.Horizontal;
            }
            else
            {
                width = width - item.Width - item.Margin.Horizontal;
            }
        }

        if (springBoxCount > 1) width /= springBoxCount;

        if (width < DefaultSize.Width) width = DefaultSize.Width;

        Size size = base.GetPreferredSize(constrainingSize);
        size.Width = width;
        return size;
    }

    protected override void OnKeyPress(KeyPressEventArgs e)
    {
        if (e.KeyChar == (char)Keys.Enter)
            e.Handled = true;
        base.OnKeyPress(e);
    }

    protected override void OnKeyDown(KeyEventArgs e)
    {
        if (e.KeyCode == Keys.Enter)
            EnterKeyDown?.Invoke(this, e);
        base.OnKeyDown(e);
    }


}