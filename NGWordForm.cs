﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Chatsumi
{
    public partial class NGWordForm : Form
    {
        NGWordConfig config;

        public NGWordForm(NGWordConfig config)
        {
            InitializeComponent();
            this.config = config;
        }

        public void Show(Form parent)
        {
            editWord.Items.AddRange(config.Words.ToArray());

            Show((IWin32Window)parent);

            if (config.Position != Rectangle.Empty)
            {
                Location = config.Position.Location;
                Size = config.Position.Size;
            }
            else
            {
                var x = parent.Width / 2 - Width / 2;
                var y = Math.Min(100, parent.Height / 2 - Height / 2);
                Location = parent.Location + new Size(x, y);
            }
        }

        private void Form_FormClosed(object sender, FormClosedEventArgs e)
        {
            config.Position = new Rectangle(Location, Size);
        }

        private void EditWord_ItemsChanged(object sender, EventArgs e)
        {
            config.Words.Clear();
            if (editWord.Items.Count > 0)
                config.Words.AddRange(editWord.Items.Cast<string>().ToArray());
        }
    }
}
