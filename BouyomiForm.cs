﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using FNF.Utility;
using System.Diagnostics;

namespace Chatsumi
{
    public partial class BouyomiForm : Form
    {
        BouyomiConfig config;
        Bouyomi bouyomi;

        private class VoiceData
        {
            public string Name;
            public int VoiceType;
            public override string ToString() => Name;
        }

        public BouyomiForm(BouyomiConfig config, Bouyomi bouyomi)
        {
            InitializeComponent();

            this.config = config;
            this.bouyomi = bouyomi;

            comboVoice.Items.Add(new VoiceData { Name = "デフォルト", VoiceType = (int)VoiceType.Default });
            comboVoice.Items.Add(new VoiceData { Name = "女性1", VoiceType = (int)VoiceType.Female1 });
            comboVoice.Items.Add(new VoiceData { Name = "女性2", VoiceType = (int)VoiceType.Female2 });
            comboVoice.Items.Add(new VoiceData { Name = "男性1", VoiceType = (int)VoiceType.Male1 });
            comboVoice.Items.Add(new VoiceData { Name = "男性2", VoiceType = (int)VoiceType.Male2 });
            comboVoice.Items.Add(new VoiceData { Name = "中性", VoiceType = (int)VoiceType.Imd1 });
            comboVoice.Items.Add(new VoiceData { Name = "ロボット", VoiceType = (int)VoiceType.Robot1 });
            comboVoice.Items.Add(new VoiceData { Name = "機械1", VoiceType = (int)VoiceType.Machine1 });
            comboVoice.Items.Add(new VoiceData { Name = "機械2", VoiceType = (int)VoiceType.Machine2 });
            comboVoice.Items.Add(new VoiceData { Name = "ランダム", VoiceType = 99 });


            textServerPath.Text = config.ServerPath;
            for (var i = 0; i < comboVoice.Items.Count; i++)
            {
                var item = comboVoice.Items[i] as VoiceData;
                if (item.VoiceType == config.VoiceType)
                    comboVoice.SelectedIndex = i;
            }
            if (config.Speed != -1)
            {
                labelSpeedCount.Text = config.Speed.ToString();
                trackSpeed.Value = config.Speed;
                checkSpeed.Checked = true;
            }
            if (config.Tone != -1)
            {
                labelToneCount.Text = config.Tone.ToString();
                trackTone.Value = config.Tone;
                checkTone.Checked = true;
            }
            if (config.Volume != -1)
            {
                labelVolumeCount.Text = config.Volume.ToString();
                trackVolume.Value = config.Volume;
                checkVolume.Checked = true;
            }
            if (config.Count != -1)
            {
                labelCountCount.Text = config.Count.ToString();
                trackCount.Value = config.Count;
                checkCount.Checked = true;
            }
        }

        private void ButtonOpenFile_Click(object sender, EventArgs e)
        {
            var result = openBouyomi.ShowDialog();
            if (result != DialogResult.OK)
                return;
            textServerPath.Text = openBouyomi.FileName;
        }

        private void ComboVoice_SelectedIndexChanged(object sender, EventArgs e)
        {
            var data = comboVoice.SelectedItem as VoiceData;
            config.VoiceType = data.VoiceType;
        }

        private void CheckSpeed_CheckedChanged(object sender, EventArgs e)
        {
            trackSpeed.Enabled = checkSpeed.Checked;
            labelSpeed.Enabled = checkSpeed.Checked;
            labelSpeedCount.Enabled = checkSpeed.Checked;

            config.Speed = checkSpeed.Checked ? trackSpeed.Value : -1;
        }

        private void CheckTone_CheckedChanged(object sender, EventArgs e)
        {
            trackTone.Enabled = checkTone.Checked;
            labelTone.Enabled = checkTone.Checked;
            labelToneCount.Enabled = checkTone.Checked;

            config.Tone = checkTone.Checked ? trackTone.Value : -1;
        }

        private void CheckVolume_CheckedChanged(object sender, EventArgs e)
        {
            trackVolume.Enabled = checkVolume.Checked;
            labelVolume.Enabled = checkVolume.Checked;
            labelVolumeCount.Enabled = checkVolume.Checked;

            config.Volume = checkVolume.Checked ? trackVolume.Value : -1;
        }

        private void CheckCount_CheckedChanged(object sender, EventArgs e)
        {
            trackCount.Enabled = checkCount.Checked;
            labelCount.Enabled = checkCount.Checked;
            labelCountCount.Enabled = checkCount.Checked;

            config.Count = checkCount.Checked ? trackCount.Value : -1;
        }

        private void TrackSpeed_Scroll(object sender, EventArgs e)
        {
            labelSpeedCount.Text = trackSpeed.Value.ToString();
            config.Speed = trackSpeed.Value;
        }

        private void TrackTone_Scroll(object sender, EventArgs e)
        {
            labelToneCount.Text = trackTone.Value.ToString();
            config.Tone = trackTone.Value;
        }

        private void TrackVolume_Scroll(object sender, EventArgs e)
        {
            labelVolumeCount.Text = trackVolume.Value.ToString();
            config.Volume = trackVolume.Value;
        }

        private void trackCount_Scroll(object sender, EventArgs e)
        {
            labelCountCount.Text = trackCount.Value.ToString();
            config.Count = trackCount.Value;
        }

        public void Show(Form parent)
        {
            Show((IWin32Window)parent);
            if (config.Posision.IsEmpty)
            {
                var x = parent.Width / 2 - Width / 2;
                var y = Math.Min(100, parent.Height / 2 - Height / 2);
                Location = parent.Location + new Size(x, y);
            }
            else
            {
                Location = config.Posision;
            }
        }

        private void Form_FormClosed(object sender, FormClosedEventArgs e)
        {
            config.Posision = Location;
        }

        private void ButtonTest_Click(object sender, EventArgs e)
        {
            if (!bouyomi.StartServer())
            {
                MsgBox.Show(this, MsgBoxIcon.Caution, MsgBoxButton.OK, MsgBoxSound.Asterisk,
                    "エラー", "棒読みちゃんを起動出来ませんでした。");
                return;
            }
            bouyomi.Speak("音声読み上げテストです");
        }

        private void TextServerPath_TextChanged(object sender, EventArgs e)
        {
            config.ServerPath = textServerPath.Text;
        }
    }
}
