﻿namespace Chatsumi
{
    partial class LogForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LogForm));
            this.toolSearch = new FlatToolStrip();
            this.textSearch = new System.Windows.Forms.ToolStripTextBox();
            this.buttonSearchBack = new System.Windows.Forms.ToolStripButton();
            this.buttonSearchNext = new System.Windows.Forms.ToolStripButton();
            this.toolMain = new FlatToolStrip();
            this.buttonSearch = new System.Windows.Forms.ToolStripButton();
            this.toolSearch.SuspendLayout();
            this.toolMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolSearch
            // 
            this.toolSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.toolSearch.AutoSize = false;
            this.toolSearch.Dock = System.Windows.Forms.DockStyle.None;
            this.toolSearch.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolSearch.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.textSearch,
            this.buttonSearchBack,
            this.buttonSearchNext});
            this.toolSearch.Location = new System.Drawing.Point(126, 25);
            this.toolSearch.Name = "toolSearch";
            this.toolSearch.Size = new System.Drawing.Size(202, 25);
            this.toolSearch.TabIndex = 1;
            this.toolSearch.Text = "検索";
            // 
            // textSearch
            // 
            this.textSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textSearch.Name = "textSearch";
            this.textSearch.Size = new System.Drawing.Size(150, 25);
            this.textSearch.ToolTipText = "検索ワード";
            this.textSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextSearch_KeyDown);
            this.textSearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextSearch_KeyPress);
            // 
            // buttonSearchBack
            // 
            this.buttonSearchBack.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonSearchBack.Image = global::Chatsumi.Properties.Resources.png_up_512;
            this.buttonSearchBack.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonSearchBack.Name = "buttonSearchBack";
            this.buttonSearchBack.Size = new System.Drawing.Size(23, 22);
            this.buttonSearchBack.Text = "前へ";
            this.buttonSearchBack.Click += new System.EventHandler(this.ButtonSearchBack_Click);
            // 
            // buttonSearchNext
            // 
            this.buttonSearchNext.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonSearchNext.Image = global::Chatsumi.Properties.Resources.png_down_512;
            this.buttonSearchNext.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonSearchNext.Name = "buttonSearchNext";
            this.buttonSearchNext.Size = new System.Drawing.Size(23, 22);
            this.buttonSearchNext.Text = "次へ";
            this.buttonSearchNext.Click += new System.EventHandler(this.ButtonSearchNext_Click);
            // 
            // toolMain
            // 
            this.toolMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buttonSearch});
            this.toolMain.Location = new System.Drawing.Point(0, 0);
            this.toolMain.Name = "toolMain";
            this.toolMain.Size = new System.Drawing.Size(328, 25);
            this.toolMain.TabIndex = 0;
            // 
            // buttonSearch
            // 
            this.buttonSearch.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.buttonSearch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonSearch.Image = global::Chatsumi.Properties.Resources.png_search_512;
            this.buttonSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(23, 22);
            this.buttonSearch.Text = "検索";
            this.buttonSearch.Click += new System.EventHandler(this.ButtonSerch_Click);
            // 
            // LogForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(328, 440);
            this.Controls.Add(this.toolSearch);
            this.Controls.Add(this.toolMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LogForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.LogForm_FormClosed);
            this.Load += new System.EventHandler(this.LogForm_Load);
            this.toolSearch.ResumeLayout(false);
            this.toolSearch.PerformLayout();
            this.toolMain.ResumeLayout(false);
            this.toolMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private FlatToolStrip toolMain;
        private FlatToolStrip toolSearch;
        private System.Windows.Forms.ToolStripButton buttonSearch;
        private System.Windows.Forms.ToolStripTextBox textSearch;
        private System.Windows.Forms.ToolStripButton buttonSearchBack;
        private System.Windows.Forms.ToolStripButton buttonSearchNext;
    }
}