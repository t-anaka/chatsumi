﻿namespace Chatsumi
{
    partial class WebScoketForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WebScoketForm));
            this.textHost = new System.Windows.Forms.TextBox();
            this.textPort = new System.Windows.Forms.TextBox();
            this.labelHost = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonEnter = new System.Windows.Forms.Button();
            this.groupWebSocket = new System.Windows.Forms.GroupBox();
            this.groupWebSocket.SuspendLayout();
            this.SuspendLayout();
            // 
            // textHost
            // 
            this.textHost.Location = new System.Drawing.Point(44, 22);
            this.textHost.MaxLength = 256;
            this.textHost.Name = "textHost";
            this.textHost.ReadOnly = true;
            this.textHost.Size = new System.Drawing.Size(115, 19);
            this.textHost.TabIndex = 1;
            // 
            // textPort
            // 
            this.textPort.Location = new System.Drawing.Point(44, 47);
            this.textPort.MaxLength = 5;
            this.textPort.Name = "textPort";
            this.textPort.Size = new System.Drawing.Size(115, 19);
            this.textPort.TabIndex = 2;
            // 
            // labelHost
            // 
            this.labelHost.AutoSize = true;
            this.labelHost.Location = new System.Drawing.Point(6, 25);
            this.labelHost.Name = "labelHost";
            this.labelHost.Size = new System.Drawing.Size(32, 12);
            this.labelHost.TabIndex = 2;
            this.labelHost.Text = "ホスト";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "ポート";
            // 
            // buttonEnter
            // 
            this.buttonEnter.Location = new System.Drawing.Point(113, 96);
            this.buttonEnter.Name = "buttonEnter";
            this.buttonEnter.Size = new System.Drawing.Size(75, 23);
            this.buttonEnter.TabIndex = 0;
            this.buttonEnter.Text = "決定";
            this.buttonEnter.UseVisualStyleBackColor = true;
            this.buttonEnter.Click += new System.EventHandler(this.ButtonEnter_Click);
            // 
            // groupWebSocket
            // 
            this.groupWebSocket.Controls.Add(this.labelHost);
            this.groupWebSocket.Controls.Add(this.textHost);
            this.groupWebSocket.Controls.Add(this.label1);
            this.groupWebSocket.Controls.Add(this.textPort);
            this.groupWebSocket.Location = new System.Drawing.Point(12, 12);
            this.groupWebSocket.Name = "groupWebSocket";
            this.groupWebSocket.Size = new System.Drawing.Size(176, 78);
            this.groupWebSocket.TabIndex = 4;
            this.groupWebSocket.TabStop = false;
            this.groupWebSocket.Text = "WebSocketの設定";
            // 
            // WebScoketForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(203, 124);
            this.Controls.Add(this.groupWebSocket);
            this.Controls.Add(this.buttonEnter);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "WebScoketForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "拡張ツールの設定";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ServerForm_FormClosed);
            this.Load += new System.EventHandler(this.ServerForm_Load);
            this.groupWebSocket.ResumeLayout(false);
            this.groupWebSocket.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox textHost;
        private System.Windows.Forms.TextBox textPort;
        private System.Windows.Forms.Label labelHost;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonEnter;
        private System.Windows.Forms.GroupBox groupWebSocket;
    }
}