﻿using CefSharp.Handler;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CefSharp
{
    public class ResourceLoadEventArgs : EventArgs
    {
        public IRequest Request { get; set; }
        public MemoryStreamResponseFilter Response { get; set; }
    }

    public class CustomResourceRequestHandler : ResourceRequestHandler
    {
        public event EventHandler<ResourceLoadEventArgs> ResourceLoadComplete;

        private Dictionary<ulong, MemoryStreamResponseFilter> responseDictionary = new Dictionary<ulong, MemoryStreamResponseFilter>();

        protected override IResponseFilter GetResourceResponseFilter(IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame, IRequest request, IResponse response)
        {
            var dataFilter = new MemoryStreamResponseFilter();
            responseDictionary.Add(request.Identifier, dataFilter);
            return dataFilter;
        }

        protected override void OnResourceLoadComplete(IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame, IRequest request, IResponse response, UrlRequestStatus status, long receivedContentLength)
        {
            MemoryStreamResponseFilter filter;

            if (responseDictionary.TryGetValue(request.Identifier, out filter))
            {
                ResourceLoadComplete?.Invoke(this, new ResourceLoadEventArgs
                {
                    Request = request,
                    Response = filter,
                });

                responseDictionary.Remove(request.Identifier);
            }

            base.OnResourceLoadComplete(chromiumWebBrowser, browser, frame, request, response, status, receivedContentLength);
        }
    }

    public class MemoryStreamResponseFilter : IResponseFilter
    {
        public byte[] Data { get; private set; } = new byte[0];
        private MemoryStream memoryStream;

        bool IResponseFilter.InitFilter()
        {
            memoryStream = new MemoryStream();
            return true;
        }

        FilterStatus IResponseFilter.Filter(Stream dataIn, out long dataInRead, Stream dataOut, out long dataOutWritten)
        {
            if (dataIn == null)
            {
                dataInRead = 0;
                dataOutWritten = 0;
                return FilterStatus.Done;
            }

            if (dataIn.Length > dataOut.Length)
            {
                var data = new byte[dataOut.Length];
                dataIn.Seek(0, SeekOrigin.Begin);
                dataIn.Read(data, 0, data.Length);
                dataOut.Write(data, 0, data.Length);

                dataInRead = dataOut.Length;
                dataOutWritten = dataOut.Length;
                return FilterStatus.NeedMoreData;
            }

            dataInRead = dataIn.Length;
            dataOutWritten = Math.Min(dataInRead, dataOut.Length);

            dataIn.CopyTo(dataOut);

            dataIn.Position = 0;
            dataIn.CopyTo(memoryStream);

            Data = memoryStream.ToArray();

            return FilterStatus.Done;
        }

        void IDisposable.Dispose()
        {
            memoryStream.Dispose();
            memoryStream = null;
        }
    }

}
