﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CefSharp;
using CefSharp.WinForms;
using CefSharp.Handler;
using CefSharp.SchemeHandler;
using System.IO;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;
using System.Net;

namespace CefSharp
{
    public class Chromium : IDisposable
    {
        public event EventHandler<FrameLoadStartEventArgs> FrameLoadStart;
        public event EventHandler<FrameLoadEndEventArgs> FrameLoadEnd;
        public event EventHandler<BeforPopupEventArgs> BeforPopup;
        public event EventHandler<OpenUrlFromTabEventArgs> OpenUrlFromTab;
        public event EventHandler<JSDialogArgs> JSDialog;

        public ChromiumWebBrowser WebBrowser { get; set; }
        public bool IsBrowserInitialized => WebBrowser == null ? false : WebBrowser.IsBrowserInitialized;
        public string Title { get; private set; } = "";

        public static void Initialize() => Initialize("cache");
        public static void Initialize(string cachePath)
        {
            if (!Directory.Exists(cachePath))
                Directory.CreateDirectory(cachePath);

            CefSharpSettings.SubprocessExitIfParentProcessClosed = true;
            CefSharpSettings.ShutdownOnExit = true;

            var settings = new CefSettings();
            settings.RegisterScheme(new CefCustomScheme
            {
                SchemeName = "local",
                DomainName = "app",
                SchemeHandlerFactory = new CustomSchemeHandlerFactory("./"),
            });

            settings.LogSeverity = LogSeverity.Disable;
            settings.CachePath = cachePath;
            settings.Locale = "ja";

            Cef.Initialize(settings);
        }

        public async Task WaitInitialize(TimeSpan timeSpan)
        {
            await Task.Run(() =>
            {
                var limit = DateTime.Now + timeSpan;
                while (DateTime.Now < limit)
                {
                    if (IsBrowserInitialized)
                        break;
                    Thread.Sleep(10);
                }
            });
        }

        public static void Shutdown()
        {
            Cef.Shutdown();
        }

        public void Init(Control control, string url = null)
        {
            if (string.IsNullOrEmpty(url))
                url = "about:blank";

            WebBrowser = new ChromiumWebBrowser(url);
            WebBrowser.Dock = DockStyle.Fill;

            var requestHandler = new CustomRequestHandler();
            requestHandler.OpenUrlFromTab += (a, b) => OpenUrlFromTab?.Invoke(a, b);
            WebBrowser.RequestHandler = requestHandler;

            var lifeSpanHandler = new CustomLifeSpanHandler();
            lifeSpanHandler.BeforPopup += (a, b) => BeforPopup?.Invoke(a, b);
            WebBrowser.LifeSpanHandler = lifeSpanHandler;

            var jsDialogHandler = new CustomJsDialogHandler();
            jsDialogHandler.JSDialog += (a, b) => JSDialog?.Invoke(a, b);
            WebBrowser.JsDialogHandler = jsDialogHandler;

            WebBrowser.TitleChanged += (a, b) => Title = b.Title;
            WebBrowser.FrameLoadStart += (a, b) => FrameLoadStart?.Invoke(a, b);
            WebBrowser.FrameLoadEnd += (a, b) => FrameLoadEnd?.Invoke(a, b);

            control.Controls.Add(WebBrowser);
            WebBrowser.BringToFront();
        }

        public void Dispose()
        {
            try
            {
                WebBrowser.Dispose();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        public void Load(string url)
        {
            WebBrowser?.Load(url);
        }

        public void Back()
        {
            if (!IsBrowserInitialized)
                return;
            WebBrowser?.Back();
        }

        public void Forward()
        {
            if (!IsBrowserInitialized)
                return;
            WebBrowser?.Forward();
        }

        public void Reload()
        {
            if (!IsBrowserInitialized)
                return;
            WebBrowser?.Reload();
        }

        public async Task<object> EvaluateScriptAsync(string methodName)
        {
            if (!IsBrowserInitialized)
                return "";
            var response = await WebBrowser?.GetMainFrame().EvaluateScriptAsync(methodName);
            return response.Result;
        }

        public void ExecuteJavaScriptAsync(string methodName)
        {
            if (!IsBrowserInitialized)
                return;
            WebBrowser?.GetMainFrame().ExecuteJavaScriptAsync(methodName);
        }

        public void SetZoom(double zoom)
        {
            if (!IsBrowserInitialized)
                return;
            WebBrowser.SetZoomLevel(Math.Log(zoom, 1.2));
        }
    }
}
