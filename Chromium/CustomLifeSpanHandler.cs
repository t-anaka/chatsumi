﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CefSharp
{
    public class BeforPopupEventArgs
    {
        public string TargetUrl { get; set; }
        public bool Cancel { get; set; }
    }

    public class CustomLifeSpanHandler : ILifeSpanHandler
    {
        public event EventHandler<BeforPopupEventArgs> BeforPopup;

        public bool DoClose(IWebBrowser browserControl, IBrowser browser)
        {
            return false;
        }

        public void OnAfterCreated(IWebBrowser browserControl, IBrowser browser)
        {
        }

        public void OnBeforeClose(IWebBrowser browserControl, IBrowser browser)
        {
        }

        public bool OnBeforePopup(IWebBrowser browserControl, IBrowser browser, IFrame frame, string targetUrl, string targetFrameName, WindowOpenDisposition targetDisposition, bool userGesture, IPopupFeatures popupFeatures, IWindowInfo windowInfo, IBrowserSettings browserSettings, ref bool noJavascriptAccess, out IWebBrowser newBrowser)
        {
            var args = new BeforPopupEventArgs
            {
                TargetUrl = targetUrl,
                Cancel = false,
            };

            BeforPopup?.Invoke(this, args);

            newBrowser = null;
            return args.Cancel;
        }
    }
}
