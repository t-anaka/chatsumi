﻿using CefSharp.Handler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CefSharp
{
    public class CustomRequestHandlerEx : CustomRequestHandler
    {
        public event EventHandler<ResourceLoadEventArgs> ResourceLoadComplete;

        CustomResourceRequestHandler resourceRequestHandler;

        public CustomRequestHandlerEx() : base()
        {
            resourceRequestHandler = new CustomResourceRequestHandler();
            resourceRequestHandler.ResourceLoadComplete += (a, b) => ResourceLoadComplete?.Invoke(a, b);
        }

        protected override IResourceRequestHandler GetResourceRequestHandler(IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame, IRequest request, bool isNavigation, bool isDownload, string requestInitiator, ref bool disableDefaultHandling)
        {
            return resourceRequestHandler;
        }
    }
}
