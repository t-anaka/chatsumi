﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CefSharp
{
    public class CustomSchemeHandlerFactory : ISchemeHandlerFactory
    {
        string rootFolder;
        string schemeName;
        string hostName;
        string defaultPage;
        FileShare resourceFileShare;

        public CustomSchemeHandlerFactory(string rootFolder, string schemeName = null, string hostName = null, string defaultPage = "index.html", FileShare resourceFileShare = FileShare.Read)
        {
            this.rootFolder = Path.GetFullPath(rootFolder);
            this.schemeName = schemeName;
            this.hostName = hostName;
            this.defaultPage = defaultPage;
            this.resourceFileShare = resourceFileShare;

            if (!Directory.Exists(this.rootFolder))
            {
                throw new DirectoryNotFoundException(this.rootFolder);
            }
        }

        IResourceHandler ISchemeHandlerFactory.Create(IBrowser browser, IFrame frame, string schemeName, IRequest request)
        {
            var url = new Uri(request.Url);
            var pattern = $"^{url.Scheme}://{url.Host}/";
            var filePath = Regex.Replace(HttpUtility.UrlDecode(request.Url), pattern, "");

            if (File.Exists(filePath))
            {
                var fileExtension = Path.GetExtension(filePath);
                var text = File.ReadAllText(filePath);
                return ResourceHandler.FromString(text, fileExtension);
            }

            return ResourceHandler.ForErrorMessage($"NotFound:{filePath}", HttpStatusCode.NotFound);
        }
    }

}
