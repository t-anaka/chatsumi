﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CefSharp
{
    public class JSDialogArgs : EventArgs
    {
        public string OriginUrl { get; set; }
        public CefJsDialogType DialogType { get; set; }
        public string MessageText { get; set; }
        public string DefaultPromptText { get; set; }
        public bool Cancel { get; set; }
    }

    public class CustomJsDialogHandler : IJsDialogHandler
    {
        public event EventHandler<JSDialogArgs> JSDialog;

        public bool OnBeforeUnloadDialog(IWebBrowser chromiumWebBrowser, IBrowser browser, string messageText, bool isReload, IJsDialogCallback callback)
        {
            return true;
        }

        public void OnDialogClosed(IWebBrowser chromiumWebBrowser, IBrowser browser)
        {
        }

        public bool OnJSDialog(IWebBrowser chromiumWebBrowser, IBrowser browser, string originUrl, CefJsDialogType dialogType, string messageText, string defaultPromptText, IJsDialogCallback callback, ref bool suppressMessage)
        {
            var arg = new JSDialogArgs
            {
                OriginUrl = originUrl,
                DialogType = dialogType,
                MessageText = messageText,
                DefaultPromptText = defaultPromptText,
                Cancel = false,
            };
            JSDialog?.Invoke(browser, arg);
            suppressMessage = arg.Cancel;
            return !arg.Cancel;
        }

        public void OnResetDialogState(IWebBrowser chromiumWebBrowser, IBrowser browser)
        {
        }
    }
}
