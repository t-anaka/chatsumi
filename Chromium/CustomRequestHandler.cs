﻿using CefSharp.Handler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CefSharp
{
    public class OpenUrlFromTabEventArgs
    {
        public string TargetUrl { get; set; }
        public bool Cancel { get; set; }
    }

    public class CustomRequestHandler : RequestHandler
    {
        public event EventHandler<OpenUrlFromTabEventArgs> OpenUrlFromTab;

        protected override bool OnOpenUrlFromTab(IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame, string targetUrl, WindowOpenDisposition targetDisposition, bool userGesture)
        {
            var args = new OpenUrlFromTabEventArgs
            {
                TargetUrl = targetUrl,
                Cancel = false,
            };

            OpenUrlFromTab?.Invoke(this, args);

            return args.Cancel;
        }
    }
}
