﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Chatsumi
{
    public class FileUtil
    {
        public static void Error(string path, string text)
        {
            var dir = Path.GetDirectoryName(path);
            var file = Path.GetFileNameWithoutExtension(path);
            var ext = Path.GetExtension(path);
            var date = DateTime.Now.ToString("yyyyMMddHHmmss");

            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);

            File.WriteAllText(Path.Combine(dir, $"{file}_{date}{ext}"), text);
        }
    }
}
