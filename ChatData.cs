﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chatsumi
{
    public enum ChatType
    {
        Connect, Comment
    }

    public class ChatData
    {
        public ChatType Type { get; set; }
        public object Data { get; set; }
    }

    public class ConnectData
    {
        public DateTime Time { get; set; }
        public string Id { get; set; }
        public string Title { get; set; }
        public string Image { get; set; }
    }
}
