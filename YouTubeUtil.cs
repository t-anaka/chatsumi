﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Net;
using System.Web;

namespace Chatsumi
{
    public class YouTubeUtil
    {
        public ConnectData GetConnectData(string url)
        {
            if (!Uri.IsWellFormedUriString(url, UriKind.Absolute))
                return null;

            if (!url.StartsWith("https://www.youtube.com/"))
                return null;

            var html = "";
            using (var web = new WebClient())
            {
                web.Encoding = Encoding.UTF8;
                html = web.DownloadString(url);
            }

            var doc = new HtmlDocument();
            doc.LoadHtml(html);

            var title = GetAttributeValue(doc, "/html/head/meta[@property='og:title']", "content");
            if (title == "")
                return null;
            title = title.Replace(" - YouTube", "");

            var image = GetAttributeValue(doc, "/html/head/meta[@property='og:image']", "content");
            if (image == "")
                return null;

            var split = image.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            if (split.Length < 5)
                return null;
            var id = split[3];

            return new ConnectData
            {
                Id = id,
                Title = title,
                Image = image,
            };
        }

        private string GetAttributeValue(HtmlDocument doc, string xpath, string name)
        {
            var node = doc.DocumentNode.SelectSingleNode(xpath);
            if (node == null)
                return "";
            var value = node.GetAttributeValue(name, "");
            return HttpUtility.HtmlDecode(value);
        }

        public string GetChatUrl(string id)
        {
            return $"https://www.youtube.com/live_chat?is_popout=1&v={id}";
        }
        public string GetLiveUrl(string id)
        {
            return $"https://www.youtube.com/watch?v={id}";
        }
        public string GetShortUrl(string id)
        {
            return $"https://youtu.be/{id}";
        }
        public string GetEmbedUrl(string id)
        {
            return $"https://www.youtube.com/embed/{id}";
        }
    }
}
