﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Chatsumi
{
    public class SyncTimer : IDisposable
    {
        enum Status
        {
            Start,
            Stop,
            Stopped,
        }

        public event EventHandler<System.Timers.ElapsedEventArgs> Elapsed;

        System.Timers.Timer timer = new System.Timers.Timer();
        Status status = Status.Stopped;

        public double Interval { get => timer.Interval; set => timer.Interval = value; }
        public bool IsRunning => status == Status.Start;

        public SyncTimer()
        {
            timer.Interval = 1000;
            timer.Elapsed += Timer_Elapsed;
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                timer.Stop();
                if (status == Status.Stop)
                {
                    status = Status.Stopped;
                    return;
                }
                Elapsed?.Invoke(this, e);
            }
            finally
            {
                switch (status)
                {
                    case Status.Start:
                        timer.Start();
                        break;
                    case Status.Stop:
                        status = Status.Stopped;
                        break;
                }
            }
        }

        public void Start()
        {
            status = Status.Start;
            timer.Start();
        }

        public async Task Stop() => await Stop(TimeSpan.FromSeconds(10));

        public async Task Stop(TimeSpan timeout)
        {
            if (status == Status.Stopped)
                return;
            status = Status.Stop;
            await Task.Run(() =>
            {
                try
                {
                    var limit = DateTime.Now + timeout;
                    while (DateTime.Now < limit)
                    {
                        if (status == Status.Stopped)
                            break;
                        Thread.Sleep(10);
                    }
                }
                catch { }
            });
        }

        public void Dispose()
        {
            var task = Stop();
            timer.Dispose();
        }
    }
}
