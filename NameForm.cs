﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Chatsumi
{
    public partial class NameForm : Form
    {
        PickupNameConfig config;

        public NameForm(PickupNameConfig config)
        {
            InitializeComponent();
            this.config = config;
        }

        private void Form_FormClosed(object sender, FormClosedEventArgs e)
        {
            config.Position = new Rectangle(Location, Size);
        }

        public void Show(Form parent)
        {
            editName.Items.AddRange(config.Names.ToArray());
            Show((IWin32Window)parent);
            if (config.Position.IsEmpty)
            {
                var x = parent.Width / 2 - Width / 2;
                var y = Math.Min(100, parent.Height / 2 - Height / 2);
                Location = parent.Location + new Size(x, y);
            }
            else
            {
                Location = config.Position.Location;
                Size = config.Position.Size;
            }
        }

        private void EditName_ItemsChanged(object sender, EventArgs e)
        {
            config.Names.Clear();
            if (editName.Items.Count > 0)
                config.Names.AddRange(editName.Items.Cast<string>().ToArray());
        }
    }
}
