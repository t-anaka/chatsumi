﻿namespace Chatsumi
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.splitMain = new System.Windows.Forms.SplitContainer();
            this.toolMain = new FlatToolStrip();
            this.buttonPower = new System.Windows.Forms.ToolStripButton();
            this.textUrl = new ToolStripSpringTextBox();
            this.buttonComment = new System.Windows.Forms.ToolStripDropDownButton();
            this.menuUserName = new System.Windows.Forms.ToolStripMenuItem();
            this.menuOwner = new System.Windows.Forms.ToolStripMenuItem();
            this.menuModeretor = new System.Windows.Forms.ToolStripMenuItem();
            this.menuMember = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSuperChat = new System.Windows.Forms.ToolStripMenuItem();
            this.menuMembership = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSticker = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.menuEditUserName = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.menuAutoOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.menuTogglePick = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonSpeech = new System.Windows.Forms.ToolStripDropDownButton();
            this.menuSpeechOff = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSpeechAll = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSpeechPick = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.menuNGWord = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEditVoice = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.menuUseBouyomi = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEditBouyomi = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonLog = new System.Windows.Forms.ToolStripDropDownButton();
            this.menuEnableLog = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.menuOpenChatLog = new System.Windows.Forms.ToolStripMenuItem();
            this.menuOpenPickLog = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.menuOpenLogPath = new System.Windows.Forms.ToolStripMenuItem();
            this.menuZipLog = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonEtc = new System.Windows.Forms.ToolStripDropDownButton();
            this.menuTopMost = new System.Windows.Forms.ToolStripMenuItem();
            this.menuWebScoket = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.menuZoom = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCopyLiveUrl = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCopyShortUrl = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCopyImage = new System.Windows.Forms.ToolStripMenuItem();
            this.menuOpenUrl = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFont = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEditWebSocket = new System.Windows.Forms.ToolStripMenuItem();
            this.menuClearCache = new System.Windows.Forms.ToolStripMenuItem();
            this.menuConfig = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSaveConfig = new System.Windows.Forms.ToolStripMenuItem();
            this.menuLoadConfig = new System.Windows.Forms.ToolStripMenuItem();
            this.sepaDevTool = new System.Windows.Forms.ToolStripSeparator();
            this.menuDevTool = new System.Windows.Forms.ToolStripMenuItem();
            this.menuChatDevTool = new System.Windows.Forms.ToolStripMenuItem();
            this.menuPickDevTool = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.splitMain)).BeginInit();
            this.splitMain.SuspendLayout();
            this.toolMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitMain
            // 
            this.splitMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitMain.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitMain.Location = new System.Drawing.Point(0, 25);
            this.splitMain.Name = "splitMain";
            this.splitMain.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.splitMain.Size = new System.Drawing.Size(384, 436);
            this.splitMain.SplitterDistance = 120;
            this.splitMain.SplitterWidth = 8;
            this.splitMain.TabIndex = 1;
            this.splitMain.TabStop = false;
            // 
            // toolMain
            // 
            this.toolMain.AllowDrop = true;
            this.toolMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buttonPower,
            this.textUrl,
            this.buttonComment,
            this.buttonSpeech,
            this.buttonLog,
            this.buttonEtc});
            this.toolMain.Location = new System.Drawing.Point(0, 0);
            this.toolMain.Name = "toolMain";
            this.toolMain.Size = new System.Drawing.Size(384, 25);
            this.toolMain.TabIndex = 0;
            this.toolMain.Text = "flatToolStrip1";
            // 
            // buttonPower
            // 
            this.buttonPower.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonPower.Image = global::Chatsumi.Properties.Resources.png_power_512;
            this.buttonPower.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonPower.Name = "buttonPower";
            this.buttonPower.Size = new System.Drawing.Size(23, 22);
            this.buttonPower.Text = "接続";
            this.buttonPower.Click += new System.EventHandler(this.ButtonPower_Click);
            // 
            // textUrl
            // 
            this.textUrl.AllowDrop = true;
            this.textUrl.Font = new System.Drawing.Font("Yu Gothic UI", 9F);
            this.textUrl.Name = "textUrl";
            this.textUrl.Size = new System.Drawing.Size(211, 25);
            this.textUrl.ToolTipText = "配信URL";
            this.textUrl.EnterKeyDown += new System.Windows.Forms.KeyEventHandler(this.TextUrl_EnterKeyDown);
            this.textUrl.DragDropText += new System.EventHandler<string>(this.TextUrl_DragDropText);
            // 
            // buttonComment
            // 
            this.buttonComment.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonComment.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuUserName,
            this.menuOwner,
            this.menuModeretor,
            this.menuMember,
            this.menuSuperChat,
            this.menuMembership,
            this.menuSticker,
            this.toolStripSeparator4,
            this.menuEditUserName,
            this.toolStripSeparator3,
            this.menuAutoOpen,
            this.menuTogglePick});
            this.buttonComment.Image = global::Chatsumi.Properties.Resources.png_comment_512;
            this.buttonComment.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonComment.Name = "buttonComment";
            this.buttonComment.Size = new System.Drawing.Size(29, 22);
            this.buttonComment.Text = "コメント";
            this.buttonComment.ToolTipText = "コメント";
            // 
            // menuUserName
            // 
            this.menuUserName.CheckOnClick = true;
            this.menuUserName.Name = "menuUserName";
            this.menuUserName.Size = new System.Drawing.Size(213, 22);
            this.menuUserName.Text = "指定ユーザーを拾う";
            this.menuUserName.CheckedChanged += new System.EventHandler(this.MenuUserName_CheckedChanged);
            // 
            // menuOwner
            // 
            this.menuOwner.CheckOnClick = true;
            this.menuOwner.Name = "menuOwner";
            this.menuOwner.Size = new System.Drawing.Size(213, 22);
            this.menuOwner.Text = "オーナーを拾う";
            this.menuOwner.CheckedChanged += new System.EventHandler(this.MenuOwner_CheckedChanged);
            // 
            // menuModeretor
            // 
            this.menuModeretor.CheckOnClick = true;
            this.menuModeretor.Name = "menuModeretor";
            this.menuModeretor.Size = new System.Drawing.Size(213, 22);
            this.menuModeretor.Text = "モデレーターを拾う";
            this.menuModeretor.CheckedChanged += new System.EventHandler(this.MenuModeretor_CheckedChanged);
            // 
            // menuMember
            // 
            this.menuMember.CheckOnClick = true;
            this.menuMember.Name = "menuMember";
            this.menuMember.Size = new System.Drawing.Size(213, 22);
            this.menuMember.Text = "メンバーを拾う";
            this.menuMember.CheckedChanged += new System.EventHandler(this.MenuMember_CheckedChanged);
            // 
            // menuSuperChat
            // 
            this.menuSuperChat.CheckOnClick = true;
            this.menuSuperChat.Name = "menuSuperChat";
            this.menuSuperChat.Size = new System.Drawing.Size(213, 22);
            this.menuSuperChat.Text = "スーパーチャットを拾う";
            this.menuSuperChat.CheckedChanged += new System.EventHandler(this.MenuSuperChat_CheckedChanged);
            // 
            // menuMembership
            // 
            this.menuMembership.CheckOnClick = true;
            this.menuMembership.Name = "menuMembership";
            this.menuMembership.Size = new System.Drawing.Size(213, 22);
            this.menuMembership.Text = "メンバー登録を拾う";
            this.menuMembership.CheckedChanged += new System.EventHandler(this.MenuMembership_CheckedChanged);
            // 
            // menuSticker
            // 
            this.menuSticker.CheckOnClick = true;
            this.menuSticker.Name = "menuSticker";
            this.menuSticker.Size = new System.Drawing.Size(213, 22);
            this.menuSticker.Text = "ステッカーを拾う";
            this.menuSticker.CheckedChanged += new System.EventHandler(this.MenuSticker_CheckedChanged);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(210, 6);
            // 
            // menuEditUserName
            // 
            this.menuEditUserName.Name = "menuEditUserName";
            this.menuEditUserName.Size = new System.Drawing.Size(213, 22);
            this.menuEditUserName.Text = "指定ユーザーの設定";
            this.menuEditUserName.Click += new System.EventHandler(this.MenuEditUserName_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(210, 6);
            // 
            // menuAutoOpen
            // 
            this.menuAutoOpen.CheckOnClick = true;
            this.menuAutoOpen.Name = "menuAutoOpen";
            this.menuAutoOpen.Size = new System.Drawing.Size(213, 22);
            this.menuAutoOpen.Text = "ピックアップ欄を自動で開く";
            this.menuAutoOpen.CheckedChanged += new System.EventHandler(this.MenuAutoOpen_CheckedChanged);
            // 
            // menuTogglePick
            // 
            this.menuTogglePick.Name = "menuTogglePick";
            this.menuTogglePick.Size = new System.Drawing.Size(213, 22);
            this.menuTogglePick.Text = "ピックアップ欄の表示/非表示";
            this.menuTogglePick.Click += new System.EventHandler(this.MenuTogglePick_Click);
            // 
            // buttonSpeech
            // 
            this.buttonSpeech.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonSpeech.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuSpeechOff,
            this.menuSpeechAll,
            this.menuSpeechPick,
            this.toolStripSeparator6,
            this.menuNGWord,
            this.menuEditVoice,
            this.toolStripSeparator1,
            this.menuUseBouyomi,
            this.menuEditBouyomi});
            this.buttonSpeech.Image = global::Chatsumi.Properties.Resources.png_speak_512;
            this.buttonSpeech.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonSpeech.Name = "buttonSpeech";
            this.buttonSpeech.Size = new System.Drawing.Size(29, 22);
            this.buttonSpeech.Text = "読み上げ";
            this.buttonSpeech.ToolTipText = "読み上げ";
            // 
            // menuSpeechOff
            // 
            this.menuSpeechOff.Name = "menuSpeechOff";
            this.menuSpeechOff.Size = new System.Drawing.Size(171, 22);
            this.menuSpeechOff.Text = "読み上げ停止";
            this.menuSpeechOff.Click += new System.EventHandler(this.MenuSpeechOff_Click);
            // 
            // menuSpeechAll
            // 
            this.menuSpeechAll.Name = "menuSpeechAll";
            this.menuSpeechAll.Size = new System.Drawing.Size(171, 22);
            this.menuSpeechAll.Text = "チャット読み上げ";
            this.menuSpeechAll.Click += new System.EventHandler(this.MenuSpeechAll_Click);
            // 
            // menuSpeechPick
            // 
            this.menuSpeechPick.Name = "menuSpeechPick";
            this.menuSpeechPick.Size = new System.Drawing.Size(171, 22);
            this.menuSpeechPick.Text = "ピックアップ読み上げ";
            this.menuSpeechPick.Click += new System.EventHandler(this.MenuSpeechPick_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(168, 6);
            // 
            // menuNGWord
            // 
            this.menuNGWord.Name = "menuNGWord";
            this.menuNGWord.Size = new System.Drawing.Size(171, 22);
            this.menuNGWord.Text = "NGワードの設定";
            this.menuNGWord.Click += new System.EventHandler(this.MenuNGWord_Click);
            // 
            // menuEditVoice
            // 
            this.menuEditVoice.Name = "menuEditVoice";
            this.menuEditVoice.Size = new System.Drawing.Size(171, 22);
            this.menuEditVoice.Text = "音声の設定";
            this.menuEditVoice.Click += new System.EventHandler(this.MenuEditVoice_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(168, 6);
            // 
            // menuUseBouyomi
            // 
            this.menuUseBouyomi.CheckOnClick = true;
            this.menuUseBouyomi.Name = "menuUseBouyomi";
            this.menuUseBouyomi.Size = new System.Drawing.Size(171, 22);
            this.menuUseBouyomi.Text = "棒読みちゃんを使用";
            this.menuUseBouyomi.CheckedChanged += new System.EventHandler(this.MenuUseBouyomi_CheckedChanged);
            // 
            // menuEditBouyomi
            // 
            this.menuEditBouyomi.Name = "menuEditBouyomi";
            this.menuEditBouyomi.Size = new System.Drawing.Size(171, 22);
            this.menuEditBouyomi.Text = "棒読みちゃんの設定";
            this.menuEditBouyomi.Click += new System.EventHandler(this.MenuEditBouyomi_Click);
            // 
            // buttonLog
            // 
            this.buttonLog.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonLog.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuEnableLog,
            this.toolStripSeparator5,
            this.menuOpenChatLog,
            this.menuOpenPickLog,
            this.toolStripSeparator7,
            this.menuOpenLogPath,
            this.menuZipLog});
            this.buttonLog.Image = global::Chatsumi.Properties.Resources.png_log_512;
            this.buttonLog.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonLog.Name = "buttonLog";
            this.buttonLog.Size = new System.Drawing.Size(29, 22);
            this.buttonLog.Text = "ログ";
            // 
            // menuEnableLog
            // 
            this.menuEnableLog.CheckOnClick = true;
            this.menuEnableLog.Name = "menuEnableLog";
            this.menuEnableLog.Size = new System.Drawing.Size(172, 22);
            this.menuEnableLog.Text = "ログを保存する";
            this.menuEnableLog.CheckedChanged += new System.EventHandler(this.MenuEnableLog_CheckedChanged);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(169, 6);
            // 
            // menuOpenChatLog
            // 
            this.menuOpenChatLog.Name = "menuOpenChatLog";
            this.menuOpenChatLog.Size = new System.Drawing.Size(172, 22);
            this.menuOpenChatLog.Text = "チャットログを開く";
            this.menuOpenChatLog.Click += new System.EventHandler(this.MenuOpenChatLog_Click);
            // 
            // menuOpenPickLog
            // 
            this.menuOpenPickLog.Name = "menuOpenPickLog";
            this.menuOpenPickLog.Size = new System.Drawing.Size(172, 22);
            this.menuOpenPickLog.Text = "ピックアップログを開く";
            this.menuOpenPickLog.Click += new System.EventHandler(this.MenuOpenPickLog_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(169, 6);
            // 
            // menuOpenLogPath
            // 
            this.menuOpenLogPath.Name = "menuOpenLogPath";
            this.menuOpenLogPath.Size = new System.Drawing.Size(172, 22);
            this.menuOpenLogPath.Text = "ログ出力先を開く";
            this.menuOpenLogPath.Click += new System.EventHandler(this.MenuOpenLogPath_Click);
            // 
            // menuZipLog
            // 
            this.menuZipLog.Name = "menuZipLog";
            this.menuZipLog.Size = new System.Drawing.Size(172, 22);
            this.menuZipLog.Text = "過去のログを圧縮";
            this.menuZipLog.Click += new System.EventHandler(this.MenuZipLog_Click);
            // 
            // buttonEtc
            // 
            this.buttonEtc.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonEtc.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuTopMost,
            this.menuWebScoket,
            this.toolStripSeparator2,
            this.menuZoom,
            this.menuCopy,
            this.menuOpenUrl,
            this.menuFont,
            this.menuEditWebSocket,
            this.menuClearCache,
            this.menuConfig,
            this.sepaDevTool,
            this.menuDevTool});
            this.buttonEtc.Image = global::Chatsumi.Properties.Resources.png_config_512;
            this.buttonEtc.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonEtc.Name = "buttonEtc";
            this.buttonEtc.Size = new System.Drawing.Size(29, 22);
            this.buttonEtc.Text = "その他";
            // 
            // menuTopMost
            // 
            this.menuTopMost.CheckOnClick = true;
            this.menuTopMost.Name = "menuTopMost";
            this.menuTopMost.Size = new System.Drawing.Size(186, 22);
            this.menuTopMost.Text = "常に手前に表示";
            this.menuTopMost.CheckedChanged += new System.EventHandler(this.MenuTopMost_CheckedChanged);
            // 
            // menuWebScoket
            // 
            this.menuWebScoket.CheckOnClick = true;
            this.menuWebScoket.Name = "menuWebScoket";
            this.menuWebScoket.Size = new System.Drawing.Size(186, 22);
            this.menuWebScoket.Text = "拡張ツールを利用";
            this.menuWebScoket.CheckedChanged += new System.EventHandler(this.MenuWebSocket_CheckedChanged);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(183, 6);
            // 
            // menuZoom
            // 
            this.menuZoom.Name = "menuZoom";
            this.menuZoom.Size = new System.Drawing.Size(186, 22);
            this.menuZoom.Text = "ズーム";
            // 
            // menuCopy
            // 
            this.menuCopy.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuCopyLiveUrl,
            this.menuCopyShortUrl,
            this.menuCopyImage});
            this.menuCopy.Name = "menuCopy";
            this.menuCopy.Size = new System.Drawing.Size(186, 22);
            this.menuCopy.Text = "コピー";
            // 
            // menuCopyLiveUrl
            // 
            this.menuCopyLiveUrl.Name = "menuCopyLiveUrl";
            this.menuCopyLiveUrl.Size = new System.Drawing.Size(177, 22);
            this.menuCopyLiveUrl.Text = "配信URLをコピー";
            this.menuCopyLiveUrl.Click += new System.EventHandler(this.MenuCopyLiveUrl_Click);
            // 
            // menuCopyShortUrl
            // 
            this.menuCopyShortUrl.Name = "menuCopyShortUrl";
            this.menuCopyShortUrl.Size = new System.Drawing.Size(177, 22);
            this.menuCopyShortUrl.Text = "短縮URLをコピー";
            this.menuCopyShortUrl.Click += new System.EventHandler(this.MenuCopyShortUrl_Click);
            // 
            // menuCopyImage
            // 
            this.menuCopyImage.Name = "menuCopyImage";
            this.menuCopyImage.Size = new System.Drawing.Size(177, 22);
            this.menuCopyImage.Text = "サムネイルURLをコピー";
            this.menuCopyImage.Click += new System.EventHandler(this.MenuCopyImage_Click);
            // 
            // menuOpenUrl
            // 
            this.menuOpenUrl.Name = "menuOpenUrl";
            this.menuOpenUrl.Size = new System.Drawing.Size(186, 22);
            this.menuOpenUrl.Text = "配信ページを開く";
            this.menuOpenUrl.Click += new System.EventHandler(this.MenuOpenUrl_Click);
            // 
            // menuFont
            // 
            this.menuFont.Name = "menuFont";
            this.menuFont.Size = new System.Drawing.Size(186, 22);
            this.menuFont.Text = "フォントの設定";
            this.menuFont.Click += new System.EventHandler(this.MenuFont_Click);
            // 
            // menuEditWebSocket
            // 
            this.menuEditWebSocket.Name = "menuEditWebSocket";
            this.menuEditWebSocket.Size = new System.Drawing.Size(186, 22);
            this.menuEditWebSocket.Text = "拡張ツールの設定";
            this.menuEditWebSocket.Click += new System.EventHandler(this.MenuEditWebSocket_Click);
            // 
            // menuClearCache
            // 
            this.menuClearCache.Name = "menuClearCache";
            this.menuClearCache.Size = new System.Drawing.Size(186, 22);
            this.menuClearCache.Text = "ブラウザキャッシュを削除";
            this.menuClearCache.Click += new System.EventHandler(this.MenuClearCache_Click);
            // 
            // menuConfig
            // 
            this.menuConfig.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuSaveConfig,
            this.menuLoadConfig});
            this.menuConfig.Name = "menuConfig";
            this.menuConfig.Size = new System.Drawing.Size(186, 22);
            this.menuConfig.Text = "設定の保存/読込";
            // 
            // menuSaveConfig
            // 
            this.menuSaveConfig.Name = "menuSaveConfig";
            this.menuSaveConfig.Size = new System.Drawing.Size(98, 22);
            this.menuSaveConfig.Text = "保存";
            this.menuSaveConfig.Click += new System.EventHandler(this.MenuSaveConfig_Click);
            // 
            // menuLoadConfig
            // 
            this.menuLoadConfig.Name = "menuLoadConfig";
            this.menuLoadConfig.Size = new System.Drawing.Size(98, 22);
            this.menuLoadConfig.Text = "読込";
            this.menuLoadConfig.Click += new System.EventHandler(this.MenuLoadConfig_Click);
            // 
            // sepaDevTool
            // 
            this.sepaDevTool.Name = "sepaDevTool";
            this.sepaDevTool.Size = new System.Drawing.Size(183, 6);
            // 
            // menuDevTool
            // 
            this.menuDevTool.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuChatDevTool,
            this.menuPickDevTool});
            this.menuDevTool.Name = "menuDevTool";
            this.menuDevTool.Size = new System.Drawing.Size(186, 22);
            this.menuDevTool.Text = "デバッグ";
            // 
            // menuChatDevTool
            // 
            this.menuChatDevTool.Name = "menuChatDevTool";
            this.menuChatDevTool.Size = new System.Drawing.Size(126, 22);
            this.menuChatDevTool.Text = "チャット";
            this.menuChatDevTool.Click += new System.EventHandler(this.MenuChatDevTool_Click);
            // 
            // menuPickDevTool
            // 
            this.menuPickDevTool.Name = "menuPickDevTool";
            this.menuPickDevTool.Size = new System.Drawing.Size(126, 22);
            this.menuPickDevTool.Text = "ピックアップ";
            this.menuPickDevTool.Click += new System.EventHandler(this.MenuPickDevTool_Click);
            // 
            // MainForm
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 461);
            this.Controls.Add(this.splitMain);
            this.Controls.Add(this.toolMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.splitMain)).EndInit();
            this.splitMain.ResumeLayout(false);
            this.toolMain.ResumeLayout(false);
            this.toolMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private FlatToolStrip toolMain;
        private System.Windows.Forms.ToolStripButton buttonPower;
        private ToolStripSpringTextBox textUrl;
        private System.Windows.Forms.SplitContainer splitMain;
        private System.Windows.Forms.ToolStripDropDownButton buttonComment;
        private System.Windows.Forms.ToolStripMenuItem menuOwner;
        private System.Windows.Forms.ToolStripMenuItem menuModeretor;
        private System.Windows.Forms.ToolStripMenuItem menuMember;
        private System.Windows.Forms.ToolStripMenuItem menuSuperChat;
        private System.Windows.Forms.ToolStripMenuItem menuMembership;
        private System.Windows.Forms.ToolStripMenuItem menuUserName;
        private System.Windows.Forms.ToolStripDropDownButton buttonEtc;
        private System.Windows.Forms.ToolStripMenuItem menuTopMost;
        private System.Windows.Forms.ToolStripMenuItem menuDevTool;
        private System.Windows.Forms.ToolStripMenuItem menuPickDevTool;
        private System.Windows.Forms.ToolStripMenuItem menuChatDevTool;
        private System.Windows.Forms.ToolStripMenuItem menuOpenUrl;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem menuZoom;
        private System.Windows.Forms.ToolStripMenuItem menuConfig;
        private System.Windows.Forms.ToolStripMenuItem menuSaveConfig;
        private System.Windows.Forms.ToolStripMenuItem menuLoadConfig;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem menuEditUserName;
        private System.Windows.Forms.ToolStripDropDownButton buttonSpeech;
        private System.Windows.Forms.ToolStripMenuItem menuSpeechOff;
        private System.Windows.Forms.ToolStripMenuItem menuSpeechPick;
        private System.Windows.Forms.ToolStripMenuItem menuSpeechAll;
        private System.Windows.Forms.ToolStripMenuItem menuEditVoice;
        private System.Windows.Forms.ToolStripDropDownButton buttonLog;
        private System.Windows.Forms.ToolStripMenuItem menuOpenChatLog;
        private System.Windows.Forms.ToolStripMenuItem menuOpenLogPath;
        private System.Windows.Forms.ToolStripMenuItem menuEnableLog;
        private System.Windows.Forms.ToolStripMenuItem menuAutoOpen;
        private System.Windows.Forms.ToolStripMenuItem menuTogglePick;
        private System.Windows.Forms.ToolStripMenuItem menuNGWord;
        private System.Windows.Forms.ToolStripMenuItem menuEditBouyomi;
        private System.Windows.Forms.ToolStripMenuItem menuUseBouyomi;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem menuClearCache;
        private System.Windows.Forms.ToolStripMenuItem menuZipLog;
        private System.Windows.Forms.ToolStripMenuItem menuFont;
        private System.Windows.Forms.ToolStripSeparator sepaDevTool;
        private System.Windows.Forms.ToolStripMenuItem menuCopy;
        private System.Windows.Forms.ToolStripMenuItem menuCopyShortUrl;
        private System.Windows.Forms.ToolStripMenuItem menuCopyLiveUrl;
        private System.Windows.Forms.ToolStripMenuItem menuCopyImage;
        private System.Windows.Forms.ToolStripMenuItem menuWebScoket;
        private System.Windows.Forms.ToolStripMenuItem menuEditWebSocket;
        private System.Windows.Forms.ToolStripMenuItem menuSticker;
        private System.Windows.Forms.ToolStripMenuItem menuOpenPickLog;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
    }
}