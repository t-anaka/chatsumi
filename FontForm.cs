﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharp;

namespace Chatsumi
{
    public partial class FontForm : Form
    {
        private class FontData
        {
            public string Text { get; set; } = "";
            public string Name { get; set; } = "";
            public override string ToString()
            {
                return Text;
            }
        }

        Config config;
        Chromium chromium = new Chromium();

        public FontForm(Config config)
        {
            InitializeComponent();
            this.config = config;
        }

        private void FontForm_Load(object sender, EventArgs e)
        {
            comboFont.Items.Add(new FontData { Text = "デフォルト", Name = "Roboto, Arial, sans-serif" });
            foreach (var font in FontFamily.Families)
            {
                if (font.Name != "")
                    comboFont.Items.Add(new FontData { Text = font.Name, Name = font.Name });
            }
            if (config.Font.Name == "")
            {
                comboFont.SelectedIndex = 0;
            }
            else
            {
                var list = comboFont.Items.Cast<FontData>().ToList();
                var i = list.FindIndex(a => a.Name == config.Font.Name);
                comboFont.SelectedIndex = i;
            }

            chromium.Init(panelSample, Path.Combine(Application.StartupPath, "html", "font.html"));
            chromium.FrameLoadEnd += Browser_FrameLoadEnd;
        }

        private void Browser_FrameLoadEnd(object sender, CefSharp.FrameLoadEndEventArgs e)
        {
            FontData data = null;
            Invoke((MethodInvoker)(() =>
            {
                data = comboFont.SelectedItem as FontData;
            }));
            chromium.SetFont(data.Name);
        }

        private void FontForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            var data = comboFont.SelectedItem as FontData;
            config.Font.Name = data.Name;
            config.Font.Position = Location;
            chromium.Dispose();
        }

        public void Show(Form parent)
        {
            Show((IWin32Window)parent);

            if (config.Font.Position.IsEmpty)
            {
                var x = parent.Width / 2 - Width / 2;
                var y = Math.Min(100, parent.Height / 2 - Height / 2);
                Location = parent.Location + new Size(x, y);
            }
            else
            {
                Location = config.Font.Position;
            }
        }

        private void ComboFont_SelectedIndexChanged(object sender, EventArgs e)
        {
            var data = comboFont.SelectedItem as FontData;
            chromium.SetFont(data.Name);
        }
    }
}
