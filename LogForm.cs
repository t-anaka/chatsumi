﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharp;
using CefSharp.WinForms;

namespace Chatsumi
{
    public partial class LogForm : Form
    {
        Config config;
        Chromium chromium = new Chromium();
        string[] files;
        List<ToolStripButton> buttons = new List<ToolStripButton>();

        public LogForm(Config config, string title, string[] files)
        {
            InitializeComponent();
            this.config = config;
            this.files = files;
            Text = $"ログ - {title}";
            toolSearch.Visible = false;
            SetPageButton(files.Length);
        }

        private void LogForm_Load(object sender, EventArgs e)
        {
            var path = Path.Combine(Application.StartupPath, files[0]);
            chromium.Init(this, path);
            chromium.WebBrowser.FrameLoadEnd += Browser_FrameLoadEnd;
        }

        public void Show(Form parent)
        {
            Show((IWin32Window)parent);

            if (config.Log.Position.IsEmpty)
            {
                var x = parent.Width / 2 - Width / 2;
                var y = Math.Min(100, parent.Height / 2 - Height / 2);
                Location = parent.Location + new Size(x, y);
            }
            else
            {
                Location = config.Log.Position.Location;
                Size = config.Log.Position.Size;
            }
        }

        private void SetPageButton(int count)
        {
            for (var i = 0; i < count; i++)
            {
                var button = new ToolStripButton();
                button.Name = $"buttonPage{i + 1}";
                button.Tag = i;
                button.DisplayStyle = ToolStripItemDisplayStyle.Text;
                button.Text = $"{i + 1}";
                button.ToolTipText = $"{i + 1}ページ";
                button.Click += Button_Click;
                button.Checked = i == 0;
                toolMain.Items.Add(button);
                buttons.Add(button);
            }
        }

        private void Button_Click(object sender, EventArgs e)
        {
            var button = sender as ToolStripButton;
            var index = (int)button.Tag;
            var url = $"local://app/{files[index]}";
            chromium.Load(url);

            for (var i = 0; i < buttons.Count; i++)
            {
                buttons[i].Checked = i == index;
            }
        }

        private void Browser_FrameLoadEnd(object sender, FrameLoadEndEventArgs e)
        {
            chromium.SetFont(config.Font.Name);
            chromium.SetZoom(config.Log.Zoom);
        }

        private void LogForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            chromium.Dispose();
            config.Log.Position = new Rectangle(Location, Size);
        }

        private void SearchNext()
        {
            if (textSearch.Text == "")
            {
                SearchClear();
                return;
            }
            chromium.WebBrowser.Find(0, textSearch.Text, true, false, false);
        }

        private void SearchBack()
        {
            if (textSearch.Text == "")
            {
                SearchClear();
                return;
            }
            chromium.WebBrowser.Find(0, textSearch.Text, false, false, false);
        }

        private void SearchClear()
        {
            textSearch.Text = "";
            chromium.WebBrowser.StopFinding(true);
        }

        private void ButtonSerch_Click(object sender, EventArgs e)
        {
            toolSearch.Visible = !toolSearch.Visible;
            if (toolSearch.Visible)
            {
                toolSearch.BringToFront();
            }
            else
            {
                SearchClear();
            }
        }

        private void TextSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                SearchNext();
        }

        private void TextSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
                e.Handled = true;
        }

        private void ButtonSearchBack_Click(object sender, EventArgs e)
        {
            SearchBack();
        }

        private void ButtonSearchNext_Click(object sender, EventArgs e)
        {
            SearchNext();
        }
    }
}
