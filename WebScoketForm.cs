﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Chatsumi
{
    public partial class WebScoketForm : Form
    {
        Config config;

        public WebScoketForm(Config config)
        {
            InitializeComponent();
            this.config = config;
        }

        private void ServerForm_Load(object sender, EventArgs e)
        {
            textHost.Text = config.WebSocket.Host;
            textPort.Text = config.WebSocket.Port.ToString();
        }

        private void ServerForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            config.WebSocket.Position = Location;
        }

        private void ButtonEnter_Click(object sender, EventArgs e)
        {
            config.WebSocket.Host = textHost.Text;
            int port;
            if (int.TryParse(textPort.Text, out port))
            {
                config.WebSocket.Port = port;
            }
            Close();
        }

        public void Show(Form parent)
        {
            Show((IWin32Window)parent);

            if (config.WebSocket.Position.IsEmpty)
            {
                var x = parent.Width / 2 - Width / 2;
                var y = Math.Min(100, parent.Height / 2 - Height / 2);
                Location = parent.Location + new Size(x, y);
            }
            else
            {
                Location = config.WebSocket.Position;
            }
        }
    }
}
