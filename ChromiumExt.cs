﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CefSharp;

namespace Chatsumi
{
    public static class ChromiumExt
    {
        public static void SetFont(this Chromium chromium, string font)
        {
            if (!chromium.IsBrowserInitialized)
                return;
            if (font == "")
                return;

            var script =
            "(function(){" +
            "let html = document.getElementsByTagName(\"html\")[0];" +
            $"html.style.fontFamily=\"{font}\";" +
            "})();";

            chromium.ExecuteJavaScriptAsync(script);
        }

        public static string ReplaceScript(this Chromium chromium, string script)
        {
            script = script.Replace("\\r", "\\\\r");
            script = script.Replace("\\n", "\\\\n");
            script = script.Replace("\r", "");
            script = script.Replace("\n", "\\n");
            script = script.Replace("\"", "\\\"");
            return script;
        }

        public static void SetScript(this Chromium chromium, string script)
        {
            if (!chromium.IsBrowserInitialized)
                return;
            if (script == "")
                return;

            script = chromium.ReplaceScript(script);

            var append =
            "(function(){" +
            "let body = document.getElementsByTagName(\"body\")[0];" +
            "let ele = document.createElement(\"script\");" +
            $"ele.innerHTML=\"{script}\";" +
            "body.appendChild(ele);" +
            "})();";

            chromium.ExecuteJavaScriptAsync(append);
        }
    }
}
