﻿function createHtml(node) {
    let menu = node.querySelector("#menu");
    let button = node.querySelector("#inline-action-button-container");
    let html = node.outerHTML;
    if (menu) {
        html = html.replace(menu.outerHTML, "");
    }
    if (button) {
        html = html.replace(button.outerHTML, "");
    }
    html = html.replace(/\r?\n/g, "");
    html = html.replace(/> +</g, "><");
    return html;
}

function createText(node) {
    let text = "";
    for (let i = 0; i < node.childNodes.length; i++) {
        switch (node.childNodes[i].nodeName) {
            case "#text":
                text += node.childNodes[i].nodeValue;
                break;
            case "IMG":
                let alt = node.childNodes[i].getAttribute("alt");
                text += alt.replace(/(^:)|(:$)/g, "");
                break;
            case "A":
                let href = node.childNodes[i].getAttribute("href");
                if (!href.startsWith("http")) href = "https://www.youtube.com/" + href;
                text += href;
                break;
            default:
                console.log(node.childNodes[i].nodeName);
                break;
        }
    }
    return text;
}

function createComment(node) {
    let id = node.getAttribute("id");
    let type = node.getAttribute("author-type");
    if (type === "") type = "normal";
    let photo = node.querySelector("#author-photo").getElementsByTagName("img")[0].getAttribute("src");
    let time = node.querySelector("#timestamp").innerText;
    let name = node.querySelector("#author-name").innerText.trim();
    let badge = [];
    let nodes = node.getElementsByTagName("yt-live-chat-author-badge-renderer");
    if (nodes) {
        for (let i = 0; i < nodes.length; i++) {
            let btype = nodes[i].getAttribute("type");
            let path = nodes[i].getElementsByTagName("path")[0];
            if (path) {
                let d = path.getAttribute("d");
                badge.push({ "type": btype, "src": d });
            } else {
                let img = nodes[i].getElementsByTagName("img")[0];
                if (img) {
                    let src = img.getAttribute("src");
                    badge.push({ "type": btype, "src": src });
                }
            }
        }
    }

    let text = createText(node.querySelector("#message"));
    let html = createHtml(node);

    let data = {
        "id": id,
        "type": type,
        "photo": photo,
        "time": time,
        "name": name,
        "badge": [],
        "text": text,
        "html": html,
        "amount": "",
        "sticker": ""
    };

    for (let i = 0; i < badge.length; i++) {
        data.badge.push(badge[i]);
    }

    return data;
}

function createSuperchat(node) {
    let id = node.getAttribute("id");
    let type = "superchat";
    let photo = node.querySelector("#author-photo").getElementsByTagName("img")[0].getAttribute("src");
    let time = node.querySelector("#timestamp").innerText;
    let name = node.querySelector("#author-name").innerText.trim();
    let amount = node.querySelector("#purchase-amount").innerText;
    let text = createText(node.querySelector("#message"));
    if (!text) text = "";
    let html = createHtml(node);

    let data = {
        "id": id,
        "type": type,
        "photo": photo,
        "time": time,
        "name": name,
        "badge": [],
        "text": text,
        "html": html,
        "amount": amount,
        "sticker": ""
    };
    return data;
}

function createMembership(node) {
    let id = node.getAttribute("id");
    let type = "membership";
    let photo = node.querySelector("#author-photo").getElementsByTagName("img")[0].getAttribute("src");
    let time = node.querySelector("#timestamp").innerText;
    let name = node.querySelector("#author-name").innerText.trim();
    let text = node.querySelector("#header-subtext").innerText;
    let html = createHtml(node);

    let data = {
        "id": id,
        "type": type,
        "photo": photo,
        "time": time,
        "name": name,
        "badge": [],
        "text": name + " " + text,
        "html": html,
        "amount": "",
        "sticker": ""
    };
    return data;
}

function createSticker(node) {
    let id = node.getAttribute("id");
    let type = "sticker";
    let photo = node.querySelector("#author-photo").getElementsByTagName("img")[0].getAttribute("src");
    let time = node.querySelector("#timestamp").innerText;
    let name = node.querySelector("#author-name").innerText.trim();
    let st_img = node.querySelector("#sticker").getElementsByTagName("img")[0];
    let sticker = st_img.getAttribute("src");
    if (!sticker.startsWith("http")) {
        sticker = "https:" + sticker;
        st_img.setAttribute("src", sticker);
    }
    let amount = node.querySelector("#purchase-amount-chip").innerText;
    let text = st_img.getAttribute("alt");
    let html = createHtml(node);

    let data = {
        "id": id,
        "type": type,
        "photo": photo,
        "time": time,
        "name": name,
        "badge": [],
        "text": text,
        "html": html,
        "amount": amount,
        "sticker": sticker
    };
    return data;
}

function getChat(html) {
    let nodes;
    if (html) {
        let div = document.createElement("div");
        div.innerHTML = html;
        nodes = div.childNodes;
    } else {
        let app = document.getElementsByTagName("yt-live-chat-app")[0];
        let items = app.querySelector("#chat").querySelector("#items");
        nodes = items.childNodes;
    }
    let datas = [];
    if (nodes) {
        for (let i = 0; i < nodes.length; i++) {
            let data = null;
            try {
                switch (nodes[i].nodeName) {
                    case "YT-LIVE-CHAT-TEXT-MESSAGE-RENDERER":
                        data = createComment(nodes[i]);
                        break;
                    case "YT-LIVE-CHAT-PAID-MESSAGE-RENDERER":
                        data = createSuperchat(nodes[i]);
                        break;
                    case "YT-LIVE-CHAT-MEMBERSHIP-ITEM-RENDERER":
                        data = createMembership(nodes[i]);
                        break;
                    case "YT-LIVE-CHAT-PAID-STICKER-RENDERER":
                        data = createSticker(nodes[i]);
                        break;
                    case "YT-LIVE-CHAT-VIEWER-ENGAGEMENT-MESSAGE-RENDERER":
                    case "YT-LIVE-CHAT-PLACEHOLDER-ITEM-RENDERER":
                    case "#text":
                        break;
                    default:
                        console.log(nodes[i].nodeName);
                        console.log(nodes[i].outerHTML);
                        break;
                }
            } catch (e) {
                console.log(e.message);
                console.log(nodes[i].outerHTML);
                continue;
            }
            if (data) {
                datas.push(data);
            }
        }
    }
    return JSON.stringify(datas);
}

function getCss() {
    let elems = document.getElementsByTagName("link");
    for (let i = 0; i < elems.length; i++) {
        let name = elems[i].getAttribute("name");
        if (!name)
            continue;
        if (name.startsWith("live_chat_polymer")) {
            let html = null;
            let url = elems[i].getAttribute("href");
            let req = new XMLHttpRequest();
            req.open("GET", url, false);
            req.onreadystatechange = function () {
                if (req.readyState === 4 && req.status === 200) {
                    html = req.responseText;
                }
            };
            req.send();
            if (html) {
                let p = document.createElement("p");
                p.innerHTML = html;
                let style = p.querySelector("style");
                return style.innerHTML;
            }
            break;
        }
    }
}