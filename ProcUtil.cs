﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Chatsumi
{
    public class ProcUtil
    {
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        private struct PROCESS_BASIC_INFORMATION
        {
            public IntPtr ExitStatus;
            public IntPtr PebBaseAddress;
            public IntPtr AffinityMask;
            public IntPtr BasePriority;
            public UIntPtr UniqueProcessId;
            public IntPtr InheritedFromUniqueProcessId;

            public int Size
            {
                get { return (int)Marshal.SizeOf(typeof(PROCESS_BASIC_INFORMATION)); }
            }
        }

        [DllImport("ntdll.dll")]
        private static extern int NtQueryInformationProcess(IntPtr processHandle, int processInformationClass, ref PROCESS_BASIC_INFORMATION processInformation, int processInformationLength, out int returnLength);

        public static Process GetParentProcess(IntPtr handle)
        {
            var info = new PROCESS_BASIC_INFORMATION();
            int length;
            int result = NtQueryInformationProcess(handle, 0, ref info, info.Size, out length);
            if (result != 0)
                return null;

            try
            {
                return Process.GetProcessById(info.InheritedFromUniqueProcessId.ToInt32());
            }
            catch
            {
                return null;
            }
        }

        public static void KillZonbieProcess(string processName)
        {
            foreach (var proc in Process.GetProcessesByName(processName))
            {
                var parent = GetParentProcess(proc.Handle);
                if (parent == null)
                {
                    Debug.WriteLine($"KillZonbie:{proc.ProcessName}");
                    proc.Kill();
                }
            }
        }

        public static bool Exist(string processName)
        {
            var proc = Process.GetProcessesByName(processName);
            return proc.Length > 0;
        }
    }
}
